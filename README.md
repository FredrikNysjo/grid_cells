# grid_cells

Implementation of our isosurface rendering method proposed in the paper ["Clustered Grid Cell Data Structure for Isosurface Rendering"](http://wscg.zcu.cz/wscg2020/journal/E07.pdf) presented at the WSCG 2020 conference and also published in the Journal of WSCG. A pre-recording of the presentation is available [here](https://drive.google.com/file/d/10OL6Q6qku_tnhKftL4kBT8j0MVGV6IdT/view?usp=sharing) (demo starts at around 13:30 in the video).


## Paper abstract

"Active grid cells in scalar volume data are typically identified by many isosurface rendering methods when extracting another representation of the data for rendering. However, the use of grid cells themselves as rendering primitives is not extensively explored in the literature. In this paper, we propose a cluster-based data structure for storing the data of active grid cells for fast cell rasterisation via billboard splatting. Compared to previous cell rasterisation approaches, eight corner scalar values are stored with each active grid cell, so that the full volume data is not required during rendering. The grid cells can be quickly extracted and use about 37 percent memory compared to a typical efficient mesh-based representation, while supporting large grid sizes. We present further improvements such as a visibility buffer for cluster culling and EWA-based interpolation of attributes such as normals. We also show that our data structure can be used for hybrid ray tracing or path tracing to compute global illumination."


## Data structure and screenshots

![Data structure](data_structure.png?raw=true "Data structure")

![Screenshots](screenshot.png?raw=true "Screenshots")


## Build instructions for Linux

You need to install the following tools, which should be available in the distributions package manager:

    gcc/g++, CMake 3.10 (or higher), make, git

In addition, you also need a few libraries from the package manager. On for example Ubuntu 18.04 and 20.04, you can install them by

    sudo apt install libgl1-mesa-dev libxmu-dev libxi-dev libxrandr-dev libxcursor-dev libxinerama-dev

Once the tools and dependencies are installed, set the environment variable `GRID_CELLS_ROOT` to the path where this `README.md` is located, and run the provided `build.sh` script to build and run the demo application. The resulting binary `grid_cells` will be placed under `GRID_CELLS_ROOT/bin`.


## Build instructions for Windows

On Windows, you can install VSCode and the Microsof Visual C++ (MSVC) compiler via the instructions from https://code.visualstudio.com/docs/cpp/config-msvc , and then use CMake (3.14 or higher) from the command line or via the [VSCode Tools addon](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cmake-tools) to generate and build the demo application. Before running the application, you also need to set the environment variable `GRID_CELLS_ROOT` to the path where this `README.md` is located

The provided `build.sh` script can also be used from Git Bash on Windows, provided that you have CMake and the MSVC compiler installed. To change the CMake generator to match the MSVC version, you can use the `GRID_CELLS_GENERATOR` environment variable (by default, this variable is set to "Visual Studio 16 2019" in the script).


## Usage

Command line usage on Linux:

    ./grid_cells [vtk_filename]

Command line usage on Windows:

    grid_cells.exe [vtk_filename]

VTK-files can also be loaded by dragging and dropping them onto the running application window.


## Other

Pre-compiled ISPC object files (compiled with ISPC 1.12) for experimental CPU-based ray tracing of the grid cell structure are available under `./src/ispc`. This was not described in the paper, but can be tested by changing the "Ray tracing mode" option under the "Ray tracing" settings to "CPU". Currently, only primary rays are implemented in the CPU-based mode. 


## License

The code is provided under the MIT license (see `LICENSE.md`).
