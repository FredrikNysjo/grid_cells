# Third-Party Dependencies

Third-party dependencies for a simple rendering framework

## License

cJSON, GLEW, GLFW, GLM, imgui, lodepng, and OpenVR are distributed under
their own respective licensens. See `LICENSES.md` for details.
