/// @file    main.cpp
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2018 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.
///

#include "gl_utils.h"
#include "gl_state.h"
#include "vtk_utils.h"
#include "marching_cubes.h"
#include "ispc/raytrace_ispc.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <imgui.h>
#include <imgui_impl_glfw_gl3.h>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <unordered_map>

// Note: some of these settings must be changed in the shaders as well!
#define META_SIZE 8
#define CLUSTER_SIZE 16
#define MAX_CLUSTERS (1 << 20)
#define USE_VOLUME_TEXTURE 0  // Upload full volume to texture for CR comparison
#define USE_16BIT_VOXELS 0  // Convert 8-bit voxel data to 16-bit for perf. comparison
#define EXTRACT_REFERENCE_MESH 1  // Extract a Marching Cubes reference mesh
#define USE_SNORM16_VERTEX_FORMAT 1  // Store mesh vertices in SNORM16 format

struct SceneUniforms {
    glm::mat4 view;
    glm::mat4 projection;
    glm::vec2 resolution;
    float time = 0.0f;
    int frame = 0;
    HemisphereLight hemisphere;
    Light light;
};

struct DrawableUniforms {
    glm::mat4 model;
    glm::mat4 normal;
    Material material;
};

struct ClusteredGridCells {
    std::vector<glm::uvec4> blocks;
    std::vector<glm::uvec3> metacells;
    std::vector<glm::uvec2> cells;
};

struct Mesh {
    std::vector<glm::vec3> vertices;
    std::vector<int> indices;
};

struct Camera {
    glm::mat4 projection;
    glm::mat4 view;
    glm::vec3 location = glm::vec3(0.0f, 0.0f, 1.6f);
};

struct Interactor {
    cg::CgTrackball trackball;
    bool panning = false;
    glm::vec2 pan_center = glm::vec2(0.0f, 0.0f);
};

struct Window {
    int width = 1000;
    int height = 700;
    float aspect = (1000.0f / 700.0f);
    GLFWwindow *id = nullptr;
    float elapsed_time = 0.0f;
    uint32_t idle_frames = 0;
    bool update_when_idle = true;
    int swap_interval = 1;
};

struct Settings {
    glm::vec3 background_color = glm::vec3(0.2f);
    glm::vec3 material_albedo = glm::vec3(0.24, 0.80f, 0.71f);
    bool show_cells = true;
    bool show_grid = false;
    bool show_mesh = false;
    bool show_normals = false;
    bool show_cluster_id = false;
    bool freeze_visibility = false;
    bool use_ewa_pass = true;
    bool fxaa_enabled = true;
    bool gamma_enabled = true;
    bool ssao_enabled = false;
    bool shadows_enabled = false;
    bool raytracing_enabled = false;
    int32_t raytracing_mode = 0;  // 0: GPU; 1: CPU
    int32_t material = 0;
    bool show_envmap = true;
    float point_scale = 1.5f;
    float interpolation = 0.8f;
    std::string input_filename = "../data/dragon_256_opacity_uint8.vtk";
    float input_scale = 1.0f;
};

struct Context {
    Window window;
    Settings settings;

    std::unordered_map<std::string, GLuint> programs;
    std::unordered_map<std::string, GLuint> vaos;
    std::unordered_map<std::string, GLuint> buffers;
    std::unordered_map<std::string, GLuint> textures;
    std::unordered_map<std::string, GLuint> framebuffers;
    std::unordered_map<std::string, GLPipelineStateInfo> pipelines;
    std::unordered_map<std::string, GLTimerQuery> timers;

    Camera camera;
    Interactor interactor;
    SceneUniforms scene_uniforms;
    DrawableUniforms drawable_uniforms;
    cg::VolumeUInt8 volume;
    ClusteredGridCells cgc;
    Mesh mesh;
};

std::string shaderDir(void)
{
    std::string root_dir = cg::cgGetEnvSafe("GRID_CELLS_ROOT");
    if (root_dir.empty()) {
        root_dir = "..";
    }
    return root_dir + "/src/shaders/";
}

void createFramebuffer(GLuint *framebuffer, int width, int height,
                       GLuint *color_texture, GLuint *depth_texture,
                       GLuint *ao_texture, GLuint *gbuffer_texture,
                       GLuint *id_texture)
{
    glDeleteTextures(1, color_texture);
    glCreateTextures(GL_TEXTURE_2D, 1, color_texture);
    glTextureStorage2D(*color_texture, 1, GL_RGBA16F, width, height);

    glDeleteTextures(1, depth_texture);
    glCreateTextures(GL_TEXTURE_2D, 1, depth_texture);
    glTextureStorage2D(*depth_texture, 1, GL_DEPTH24_STENCIL8, width, height);

    glDeleteTextures(1, ao_texture);
    glCreateTextures(GL_TEXTURE_2D, 1, ao_texture);
    glTextureStorage2D(*ao_texture, 1, GL_R8, width, height);

    glDeleteTextures(1, gbuffer_texture);
    glCreateTextures(GL_TEXTURE_2D, 1, gbuffer_texture);
    glTextureStorage2D(*gbuffer_texture, 1, GL_RGBA16F, width, height);

    glDeleteTextures(1, id_texture);
    glCreateTextures(GL_TEXTURE_2D, 1, id_texture);
    glTextureStorage2D(*id_texture, 1, GL_RG32F, width, height);

    glDeleteFramebuffers(1, framebuffer);
    glCreateFramebuffers(1, framebuffer);
    glNamedFramebufferTexture(*framebuffer, GL_COLOR_ATTACHMENT0, *color_texture, 0);
    glNamedFramebufferTexture(*framebuffer, GL_COLOR_ATTACHMENT2, *ao_texture, 0);
    glNamedFramebufferTexture(*framebuffer, GL_COLOR_ATTACHMENT3, *gbuffer_texture, 0);
    glNamedFramebufferTexture(*framebuffer, GL_COLOR_ATTACHMENT4, *id_texture, 0);
    glNamedFramebufferTexture(*framebuffer, GL_DEPTH_STENCIL_ATTACHMENT, *depth_texture, 0);
    GLenum status = glCheckNamedFramebufferStatus(*framebuffer, GL_DRAW_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE) {
        std::fprintf(stderr, "Error: Incomplete framebuffer\n");
    }
}

void createShadowMap(GLuint *texture, int width=512, int height=512)
{
    glDeleteTextures(1, texture);
    glCreateTextures(GL_TEXTURE_2D, 1, texture);
    glTextureStorage2D(*texture, 1, GL_R32F, width, height);
}

void createVolumeTexture(GLuint *texture, const cg::VolumeUInt8 &volume)
{
    int width = volume.base.dimensions.x;
    int height = volume.base.dimensions.y;
    int depth = volume.base.dimensions.z;

    glDeleteTextures(1, texture);
    glCreateTextures(GL_TEXTURE_3D, 1, texture);
    glTextureParameteri(*texture, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTextureParameteri(*texture, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTextureParameteri(*texture, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glTextureParameteri(*texture, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTextureParameteri(*texture, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTextureStorage3D(*texture, 1, USE_16BIT_VOXELS ? GL_R16 : GL_R8, width, height, depth);
    glTextureSubImage3D(*texture, 0, 0, 0, 0, width, height, depth, GL_RED,
                        GL_UNSIGNED_BYTE, &volume.base.data[0]);
}

void createUniformBuffer(GLuint *buffer, int num_bytes=65536)
{
    glDeleteBuffers(1, buffer);
    glCreateBuffers(1, buffer);
    glBindBuffer(GL_COPY_WRITE_BUFFER, *buffer);  // Workaround for DSA issue
    glNamedBufferData(*buffer, num_bytes, nullptr, GL_DYNAMIC_DRAW);
}

void createCGCBuffers(GLuint *cell_buffer, GLuint *cell_texture,
                      GLuint *metacell_buffer, GLuint *metacell_texture,
                      GLuint *block_buffer, GLuint *block_texture,
                      const ClusteredGridCells &cgc)
{
    {
        glDeleteBuffers(1, cell_buffer);
        glCreateBuffers(1, cell_buffer);
        glBindBuffer(GL_COPY_WRITE_BUFFER, *cell_buffer);  // Workaround for DSA issue
    #if USE_16BIT_VOXELS
        std::vector<uint16_t> tmp(cgc.cells.size() * 8);
        for (uint32_t i = 0; i < cgc.cells.size(); ++i)
            for (uint32_t j = 0; j < 8; ++j)
                tmp[i * 8 + j] = (float)((uint8_t *)&cgc.cells[i])[j] * (65535.0f / 255.0f);
        glNamedBufferData(*cell_buffer, tmp.size() * sizeof(tmp[0]), &tmp[0], GL_STATIC_DRAW);
    #else
        size_t num_bytes = cgc.cells.size() * sizeof(glm::uvec2);
        glNamedBufferData(*cell_buffer, num_bytes, &cgc.cells[0], GL_STATIC_DRAW);
    #endif

        glDeleteTextures(1, cell_texture);
        glCreateTextures(GL_TEXTURE_BUFFER, 1, cell_texture);
        glTextureBuffer(*cell_texture, USE_16BIT_VOXELS ? GL_RGBA16 : GL_RGBA8, *cell_buffer);
    }
    {
        glDeleteBuffers(1, metacell_buffer);
        glCreateBuffers(1, metacell_buffer);
        glBindBuffer(GL_COPY_WRITE_BUFFER, *metacell_buffer);
        size_t num_bytes = cgc.metacells.size() * sizeof(glm::uvec3);
        glNamedBufferData(*metacell_buffer, num_bytes, &cgc.metacells[0], GL_STATIC_DRAW);

        glDeleteTextures(1, metacell_texture);
        glCreateTextures(GL_TEXTURE_BUFFER, 1, metacell_texture);
        glTextureBuffer(*metacell_texture, GL_RGB32UI, *metacell_buffer);
    }
    {
        glDeleteBuffers(1, block_buffer);
        glCreateBuffers(1, block_buffer);
        glBindBuffer(GL_COPY_WRITE_BUFFER, *block_buffer);
        size_t num_bytes = cgc.blocks.size() * sizeof(cgc.blocks[0]);
        glNamedBufferData(*block_buffer, num_bytes, &cgc.blocks[0], GL_STATIC_DRAW);

        glDeleteTextures(1, block_texture);
        glCreateTextures(GL_TEXTURE_BUFFER, 1, block_texture);
        glTextureBuffer(*block_texture, GL_RGBA32UI, *block_buffer);
    }
}

void createVisibilityBuffer(GLuint *buffer, GLuint *texture)
{
    glDeleteBuffers(1, buffer);
    glCreateBuffers(1, buffer);
    glBindBuffer(GL_COPY_WRITE_BUFFER, *buffer);  // Workaround for DSA issue
    size_t num_bytes = (MAX_CLUSTERS / 32) * sizeof(uint32_t);
    glNamedBufferData(*buffer, num_bytes, nullptr, GL_STATIC_DRAW);
    glClearNamedBufferData(*buffer, GL_R32UI, GL_RED_INTEGER, GL_UNSIGNED_INT, nullptr);

    glDeleteTextures(1, texture);
    glCreateTextures(GL_TEXTURE_BUFFER, 1, texture);
    glTextureBuffer(*texture, GL_R32UI, *buffer);
}

void createBrickBuffer(GLuint *buffer, GLuint *texture, const ClusteredGridCells &cgc)
{
    glDeleteBuffers(1, buffer);
    glCreateBuffers(1, buffer);
    glBindBuffer(GL_COPY_WRITE_BUFFER, *buffer);  // Workaround for DSA issue
    size_t num_bytes = cgc.blocks.size() * sizeof(cgc.blocks[0]);
    glNamedBufferData(*buffer, num_bytes, &cgc.blocks[0], GL_STATIC_DRAW);

    glDeleteTextures(1, texture);
    glCreateTextures(GL_TEXTURE_BUFFER, 1, texture);
    glTextureBuffer(*texture, GL_RGBA32UI, *buffer);
}

void createMarchingCubesBuffer(GLuint *vertex_buffer, GLuint *texture,
                               GLuint *index_buffer, const Mesh &mesh)
{
#if USE_SNORM16_VERTEX_FORMAT
    // Compute normalization scale factor from mesh AABB
    float scale = 1.0f;
    for (auto &v : mesh.vertices)
        scale = glm::max(scale, glm::max(glm::abs(v.x), glm::max(glm::abs(v.y), glm::abs(v.z))));
    scale = glm::ceil(scale);

    std::vector<uint32_t> tmp;
    for (auto &v : mesh.vertices) {
        // Store scale factor in unused w-component
        tmp.push_back(glm::packSnorm2x16(glm::vec2(v.x / scale, v.y / scale)));
        tmp.push_back(glm::packSnorm2x16(glm::vec2(v.z / scale , scale / 32767.0f)));
    }

    glDeleteBuffers(1, vertex_buffer);
    glCreateBuffers(1, vertex_buffer);
    glBindBuffer(GL_COPY_WRITE_BUFFER, *vertex_buffer);  // Workaround for DSA issue
    size_t num_vertex_bytes = tmp.size() * sizeof(tmp[0]);
    glNamedBufferData(*vertex_buffer, num_vertex_bytes, &tmp[0], GL_STATIC_DRAW);

    glDeleteTextures(1, texture);
    glCreateTextures(GL_TEXTURE_BUFFER, 1, texture);
    glTextureBuffer(*texture, GL_RG32UI, *vertex_buffer);
#else // USE_SNORM16_VERTEX_FORMAT
    glDeleteBuffers(1, vertex_buffer);
    glCreateBuffers(1, vertex_buffer);
    glBindBuffer(GL_COPY_WRITE_BUFFER, *vertex_buffer);
    size_t num_vertex_bytes = mesh.vertices.size() * sizeof(mesh.vertices[0]);
    glNamedBufferData(*vertex_buffer, num_vertex_bytes, &mesh.vertices[0], GL_STATIC_DRAW);

    glDeleteTextures(1, texture);
    glCreateTextures(GL_TEXTURE_BUFFER, 1, texture);
    glTextureBuffer(*texture, GL_RGB32F, *vertex_buffer);
#endif // USE_SNORM16_VERTEX_FORMAT

    glDeleteBuffers(1, index_buffer);
    glCreateBuffers(1, index_buffer);
    glBindBuffer(GL_COPY_WRITE_BUFFER, *index_buffer);
    size_t num_index_bytes = mesh.indices.size() * sizeof(mesh.indices[0]);
    glNamedBufferData(*index_buffer, num_index_bytes, &mesh.indices[0], GL_STATIC_DRAW);
}

void setupGLPrograms(Context &ctx)
{
    // Delete programs
    for (auto &pi : ctx.programs) { glDeleteProgram(pi.second); }

    ctx.programs["fxaa_gamma"] = cg::cgLoadShaderProgram(shaderDir() + "fxaa_gamma.glsl");
    ctx.programs["fixshadows_cs"] = cg::cgLoadShaderProgram(shaderDir() + "fixshadows_cs.glsl");
    ctx.programs["draw_cells"] = cg::cgLoadShaderProgram(shaderDir() + "draw_cells.glsl");
    ctx.programs["draw_cells_ewa"] = cg::cgLoadShaderProgram(shaderDir() + "draw_cells_ewa.glsl");
    ctx.programs["draw_mesh"] = cg::cgLoadShaderProgram(shaderDir() + "draw_mesh.glsl");
    ctx.programs["draw_grid"] = cg::cgLoadShaderProgram(shaderDir() + "draw_grid.glsl");
    ctx.programs["raytrace_cs"] = cg::cgLoadShaderProgram(shaderDir() + "raytrace_cs.glsl");
    ctx.programs["visibility_cs"] = cg::cgLoadShaderProgram(shaderDir() + "visibility_cs.glsl");
    ctx.programs["deferred_cs"] = cg::cgLoadShaderProgram(shaderDir() + "deferred_cs.glsl");
    ctx.programs["ao_cs"] = cg::cgLoadShaderProgram(shaderDir() + "ao_cs.glsl");
}

void setupGLPipelines(Context &ctx)
{
    glClearCurrentPipelineState();
    glUseProgram(ctx.programs["fxaa_gamma"]);
    glDepthMask(GL_FALSE);
    glCaptureCurrentPipelineState(&ctx.pipelines["fxaa_gamma"]);

    glClearCurrentPipelineState();
    glUseProgram(ctx.programs["draw_grid"]);
    glEnable(GL_PROGRAM_POINT_SIZE);
    glEnable(GL_DEPTH_TEST);
    glCaptureCurrentPipelineState(&ctx.pipelines["draw_grid"]);

    glClearCurrentPipelineState();
    glUseProgram(ctx.programs["draw_cells"]);
    glEnable(GL_PROGRAM_POINT_SIZE);
    glEnable(GL_DEPTH_TEST);
    glCaptureCurrentPipelineState(&ctx.pipelines["draw_cells"]);

    glClearCurrentPipelineState();
    glUseProgram(ctx.programs["draw_cells_ewa"]);
    glEnable(GL_PROGRAM_POINT_SIZE);
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_FALSE);
    glEnable(GL_BLEND);
    glBlendFuncSeparate(GL_ONE, GL_ONE, GL_ONE, GL_ONE);
    glCaptureCurrentPipelineState(&ctx.pipelines["draw_cells_ewa"]);

    glClearCurrentPipelineState();
    glUseProgram(ctx.programs["draw_mesh"]);
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glCaptureCurrentPipelineState(&ctx.pipelines["draw_mesh"]);

    glClearCurrentPipelineState();
}

void setupGLFramebuffers(Context &ctx)
{
    createFramebuffer(&ctx.framebuffers["forward"], ctx.window.width, ctx.window.height,
            &ctx.textures["color"], &ctx.textures["depth"],
            &ctx.textures["ao"], &ctx.textures["gbuffer"],
            &ctx.textures["id"]);

    // Not framebuffers, but create them here anyway...
    createShadowMap(&ctx.textures["shadowmap"], 512, 512);
    createShadowMap(&ctx.textures["shadowmap2"], 512, 512);
}

void setupGLBuffers(Context &ctx)
{
    createUniformBuffer(&ctx.buffers["scene_uniforms"]);
    createUniformBuffer(&ctx.buffers["drawable_uniforms"]);
}

void setupGLQueries(Context &ctx)
{
    // Delete queries
    for (auto &ti : ctx.timers) { glDeleteQueries(1, &ti.second.query); }

    glCreateQueries(GL_TIME_ELAPSED, 1, &ctx.timers["rasterise"].query);
    glCreateQueries(GL_TIME_ELAPSED, 1, &ctx.timers["visibility"].query);
    glCreateQueries(GL_TIME_ELAPSED, 1, &ctx.timers["raytrace"].query);
    glCreateQueries(GL_TIME_ELAPSED, 1, &ctx.timers["ssao"].query);
    glCreateQueries(GL_TIME_ELAPSED, 1, &ctx.timers["deferred"].query);
    glCreateQueries(GL_TIME_ELAPSED, 1, &ctx.timers["shadows"].query);
    glCreateQueries(GL_TIME_ELAPSED, 1, &ctx.timers["fxaa_gamma"].query);
}

void setupTrackball(Context &ctx)
{
    ctx.interactor.trackball.radius = double(std::min(ctx.window.width, ctx.window.height)) / 2.0;
    ctx.interactor.trackball.center = glm::vec2(ctx.window.width, ctx.window.height) / 2.0f;
}

void setupCameras(Context &ctx)
{
    ctx.camera.projection = glm::perspective(glm::radians(45.0f), ctx.window.aspect, 0.1f, 100.0f);
}

void loadVolume(Context &ctx)
{
    if (ctx.settings.input_filename != "")
        cg::volumeLoadVTK((cg::VolumeBase *)&ctx.volume, ctx.settings.input_filename);
#if USE_VOLUME_TEXTURE
    createVolumeTexture(&ctx.textures["volume"], ctx.volume);
#endif
}

void loadBlueNoiseTexture(Context &ctx)
{
    int32_t w, h, n;
    std::string filename = shaderDir() + "../../data/LDR_RGBA_0_128.png";
    uint8_t *image = stbi_load(filename.c_str(), &w, &h, &n, 4);
    glCreateTextures(GL_TEXTURE_2D, 1, &ctx.textures["bluenoise"]);
    glTextureStorage2D(ctx.textures["bluenoise"], 1, GL_RGBA8, w, h);
    glTextureSubImage2D(ctx.textures["bluenoise"], 0, 0, 0, w, h, GL_RGBA, GL_UNSIGNED_BYTE, &image[0]);
}

void loadHDREnvMap(Context &ctx)
{
    int32_t w, h, n;
    std::string filename = shaderDir() + "../../data/surgery_2k.hdr";
    float *image = stbi_loadf(filename.c_str(), &w, &h, &n, 0);
    glCreateTextures(GL_TEXTURE_2D, 1, &ctx.textures["envmap"]);
    glTextureStorage2D(ctx.textures["envmap"], 1, GL_RGB16F, w, h);
    glTextureSubImage2D(ctx.textures["envmap"], 0, 0, 0, w, h, GL_RGB, GL_FLOAT, &image[0]);
}

void mortonToXYZ(int32_t index, int32_t &x, int32_t &y, int32_t &z)
{
    x = 0, y = 0, z = 0;
    for (int32_t i = 0; i < 10; ++i) {
        x |= ((index >> (3 * i + 0)) & 1) << i;
        y |= ((index >> (3 * i + 1)) & 1) << i;
        z |= ((index >> (3 * i + 2)) & 1) << i;
    }
}

glm::ivec3 mortonToXYZCached(int32_t index)
{
    glm::ivec3 p;
    static std::vector<glm::ivec3> s_cache;
    if (index == int32_t(s_cache.size())) {
        mortonToXYZ(index, p.x, p.y, p.z);
        s_cache.push_back(p);
    }
    return s_cache[index];
}

glm::ivec2 computeBrick(cg::VolumeUInt8 &volume, int32_t xmin, int32_t xmax, int32_t ymin, int32_t ymax, int32_t zmin, int32_t zmax, ClusteredGridCells &ps, int offset)
{
    const glm::ivec3 image_dim = volume.base.dimensions;
    const glm::ivec3 brick_dim = glm::ivec3(xmax - xmin, ymax - ymin, zmax - zmin);

    const int32_t max_dim = glm::max(brick_dim.x, glm::max(brick_dim.y, brick_dim.z));
    const int32_t max_pot = glm::exp2(glm::ceil(glm::log2(float(max_dim))));
    const int32_t num_cells = max_pot * max_pot * max_pot;
    const int32_t num_metacells = num_cells / META_SIZE;
    const int32_t isovalue = 127;
    const int32_t voxel_offsets[] = { 0, 1, 3, 4, 9, 10, 12, 13 };

    glm::ivec3 aabb[2] = { glm::ivec3(999999), glm::ivec3(-999999) };
    glm::ivec2 count = glm::ivec2(0);

    for (int32_t morton = 0; morton < num_metacells; ++morton) {
        glm::ivec3 q = mortonToXYZCached(morton) * 2;
        if (glm::any(glm::greaterThan(q, brick_dim - 1)))
            continue;

        glm::ivec3 p = q + glm::ivec3(xmin, ymin, zmin);
        if (glm::any(glm::greaterThan(p, image_dim - 3)))
            continue;

        int32_t voxels[27];
        for (int32_t k = 0; k < 3; ++k)
            for (int32_t j = 0; j < 3; ++j)
                for (int32_t i = 0; i < 3; ++i)
                    voxels[k * 9 + j * 3 + i] = volume(p.x + i, p.y + j, p.z + k);

        int32_t min_val = voxels[0];
        int32_t max_val = voxels[0];
        for (int32_t j = 1; j < 27; ++j) {
            min_val = std::min(min_val, voxels[j]);
            max_val = std::max(max_val, voxels[j]);
        }

        if (min_val >= isovalue || max_val < isovalue)
            continue;

        glm::uvec3 metacell = glm::uvec3(0u);
        metacell.x = (p.x >> 1) | ((p.y >> 1) << 16);
        metacell.y = (p.z >> 1);
        metacell.z = count[1] + offset;

        for (int32_t j = 0; j < 8; ++j) {
            int32_t first = (j / 4) * 9 + ((j / 2) % 2) * 3 + (j % 2);
            int32_t c[8];
            int32_t min_val = voxels[first];
            int32_t max_val = voxels[first];
            for (int32_t i = 0; i < 8; ++i) {
                c[i] = voxels[first + voxel_offsets[i]];
                min_val = std::min(min_val, c[i]);
                max_val = std::max(max_val, c[i]);
            }

            if (min_val < isovalue && max_val >= isovalue) {
                metacell.y |= (1u << j) << 16;
                ps.cells.push_back(glm::uvec2(c[0] | (c[1] << 8) | (c[2] << 16) | (c[3] << 24),
                                              c[4] | (c[5] << 8) | (c[6] << 16) | (c[7] << 24)));
                count[1] += 1;
            }
        }
        ps.metacells.push_back(metacell);
        count[0] += 1;

        // Compute bounding box to be attached at end of cluster
        aabb[0] = glm::min(aabb[0], p / 2);
        aabb[1] = glm::max(aabb[1], p / 2);
        if ((count[0] % CLUSTER_SIZE) == (CLUSTER_SIZE - 1)) {
            glm::ivec3 a = aabb[0];
            glm::ivec3 b = aabb[1] - aabb[0];
            ps.metacells.push_back(glm::uvec3(a.x | (a.y << 16), a.z | (b.x << 16),  b.y | (b.z << 16)));
            count[0] += 1;
            aabb[0] = glm::ivec3(999999);
            aabb[1] = glm::ivec3(-999999);
        }
    }
    while (count[0] % CLUSTER_SIZE) {
        // Pad with empty metacells to keep cluster within brick
        if ((count[0] % CLUSTER_SIZE) == (CLUSTER_SIZE - 1)) {
            glm::ivec3 a = aabb[0];
            glm::ivec3 b = aabb[1] - aabb[0];
            ps.metacells.push_back(glm::uvec3(a.x | (a.y << 16), a.z | (b.x << 16),  b.y | (b.z << 16)));
        }
        else {
            ps.metacells.push_back(glm::uvec3(0u));
        }
        count[0] += 1;
    }
    count[0] /= CLUSTER_SIZE;

    return count;
}

void extractClusteredGridCells(Context &ctx)
{
    glm::ivec3 image_dim = ctx.volume.base.dimensions;
    glm::vec3 brick_dim = glm::vec3(image_dim) / 16.0f;

    ctx.cgc.metacells.clear();
    ctx.cgc.cells.clear();
    ctx.cgc.blocks.clear();

    int32_t first = 0;
    int32_t offset = 0;
    for (int32_t k = 0; k < 16; ++k) {
        for (int32_t j = 0; j < 16; ++j) {
            for (int32_t i = 0; i < 16; ++i) {
                int32_t xmin = glm::floor((i + 0) * brick_dim.x);
                int32_t xmax = glm::ceil((i + 1) * brick_dim.x);
                int32_t ymin = glm::floor((j + 0) * brick_dim.y);
                int32_t ymax = glm::ceil((j + 1) * brick_dim.y);
                int32_t zmin = glm::floor((k + 0) * brick_dim.z);
                int32_t zmax = glm::ceil((k + 1) * brick_dim.z);

                glm::ivec2 count = computeBrick(ctx.volume, xmin, xmax, ymin, ymax, zmin, zmax, ctx.cgc, offset);
                ctx.cgc.blocks.push_back(glm::uvec4(count[0], first, 0u, 0u));
                first += count[0];
                offset += count[1];
            }
        }
    }

    createCGCBuffers(&ctx.buffers["cells"], &ctx.textures["cells"],
                     &ctx.buffers["metacells"], &ctx.textures["metacells"],
                     &ctx.buffers["blocks"], &ctx.textures["blocks"], ctx.cgc);
    createVisibilityBuffer(&ctx.buffers["visibility"], &ctx.textures["visibility"]);
    createVisibilityBuffer(&ctx.buffers["visibility2"], &ctx.textures["visibility2"]);
}

void doInitialization(Context &ctx)
{
    // Initialize GL objects
    setupGLPrograms(ctx);
    setupGLPipelines(ctx);
    setupGLFramebuffers(ctx);
    setupGLBuffers(ctx);
    setupGLQueries(ctx);

    // Setup trackball and viewing
    setupTrackball(ctx);
    setupCameras(ctx);

    // Load textures
    loadBlueNoiseTexture(ctx);
    loadHDREnvMap(ctx);

    // Load volume data
    loadVolume(ctx);
    glm::ivec3 dim = ctx.volume.base.dimensions;
    glm::vec3 extent = glm::vec3(ctx.volume.base.dimensions);
    glm::vec3 aspect = extent / glm::max(extent.x, glm::max(extent.y, extent.z));
    ctx.drawable_uniforms.model =
        glm::scale(glm::mat4(), glm::vec3(ctx.settings.input_scale)) *
            glm::translate(glm::mat4(), -0.5f * aspect) *
                glm::scale(glm::mat4(), 1.0f * aspect / extent);
    ctx.drawable_uniforms.normal = glm::transpose(glm::inverse(ctx.drawable_uniforms.model));
    std::fprintf(stdout, "Input volume size: %dx%dx%d\n", dim.x, dim.y, dim.z);

    // Extract grid cell data structure
    double tic0 = glfwGetTime();
    extractClusteredGridCells(ctx);
    std::fprintf(stdout, "Cell extraction time (s): %lf\n", glfwGetTime() - tic0);

    // Extract Marching Cubes mesh (for comparison)
#if EXTRACT_REFERENCE_MESH
    double tic1 = glfwGetTime();
    marching::marchingCubesIndexed(&ctx.volume, 0.5f, &ctx.mesh.vertices, &ctx.mesh.indices);
    createMarchingCubesBuffer(&ctx.buffers["mesh"], &ctx.textures["mesh"],
                              &ctx.buffers["mesh_indices"], ctx.mesh);
    std::fprintf(stdout, "MC extraction time (s): %lf\n", glfwGetTime() - tic1);
    std::fprintf(stdout, "MC vertex count: %ld\n", ctx.mesh.vertices.size());
    std::fprintf(stdout, "MC triangle count: %ld\n", ctx.mesh.indices.size() / 3);
#endif
}

void dispatchFixShadows(Context &ctx)
{
    glBindTextureUnit(0, ctx.textures["shadowmap"]);
    glBindImageTexture(0, ctx.textures["shadowmap2"], 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R32F);

    glUseProgram(ctx.programs["fixshadows_cs"]);
    {
        glDispatchCompute(512 / 8, 512 / 8, 1);
        glMemoryBarrier(GL_ALL_BARRIER_BITS);
    }
    glUseProgram(0);
}

void dispatchDeferred(Context &ctx)
{
    glm::mat4 trackball = cg::cgTrackballGetRotationMatrix(ctx.interactor.trackball);
    glm::vec3 light_pos = glm::vec3(0.5f, 1.0f, 2.0f);
    glm::mat4 shadow = glm::perspective(glm::radians(35.0f), 1.0f, 0.5f, 3.0f) *
        glm::lookAt(light_pos, glm::vec3(0.0f), glm::vec3(0.0f, 1.0f, 0.0f)) * trackball;
    glm::mat4 shadow_from_view = shadow * glm::inverse(ctx.scene_uniforms.view);

    glBindBufferBase(GL_UNIFORM_BUFFER, 0, ctx.buffers["scene_uniforms"]);
    glBindTextureUnit(0, ctx.textures["depth"]);
    glBindImageTexture(1, ctx.textures["gbuffer"], 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA16F);
    glBindImageTexture(2, ctx.textures["id"], 0, GL_FALSE, 0, GL_READ_ONLY, GL_RG32UI);
    glBindImageTexture(3, ctx.textures["ao"], 0, GL_FALSE, 0, GL_READ_ONLY, GL_R8);
    glBindImageTexture(4, ctx.textures["shadowmap2"], 0, GL_FALSE, 0, GL_READ_ONLY, GL_R32UI);
    glBindImageTexture(5, ctx.textures["color"], 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA16F);

    glUseProgram(ctx.programs["deferred_cs"]);
    {
        glUniform3fv(0, 1, &ctx.settings.background_color[0]);
        glUniform3fv(1, 1, &ctx.settings.material_albedo[0]);
        glUniform1i(2, ctx.settings.show_normals);
        glUniform1i(3, ctx.settings.show_cluster_id);
        glUniformMatrix4fv(4, 1, GL_FALSE, &shadow_from_view[0][0]);
        glDispatchCompute(ctx.window.width / 8 + 1, ctx.window.height / 8 + 1, 1);
        glMemoryBarrier(GL_ALL_BARRIER_BITS);
    }
    glUseProgram(0);
}

void dispatchSSAO(Context &ctx)
{
    glBindBufferBase(GL_UNIFORM_BUFFER, 0, ctx.buffers["scene_uniforms"]);
    glBindTextureUnit(0, ctx.textures["depth"]);
    glBindImageTexture(1, ctx.textures["ao"], 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R8);

    glUseProgram(ctx.programs["ao_cs"]);
    {
        glDispatchCompute(ctx.window.width / 8, ctx.window.height / 8, 1);
        glMemoryBarrier(GL_ALL_BARRIER_BITS);
    }
    glUseProgram(0);
}

void dispatchVisibility(Context &ctx)
{
    glBindTextureUnit(0, ctx.textures["id"]);
    glBindImageTexture(0, ctx.textures["visibility"], 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32UI);
    glBindImageTexture(1, ctx.textures["visibility2"], 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32UI);
    glClearNamedBufferData(ctx.buffers["visibility"], GL_R32UI, GL_RED_INTEGER, GL_UNSIGNED_INT, nullptr);
    glClearNamedBufferData(ctx.buffers["visibility2"], GL_R32UI, GL_RED_INTEGER, GL_UNSIGNED_INT, nullptr);

    glUseProgram(ctx.programs["visibility_cs"]);
    {
        glDispatchCompute(ctx.window.width / 8, ctx.window.height / 8, 1);
        glMemoryBarrier(GL_ALL_BARRIER_BITS);
    }
    glUseProgram(0);
}

void drawClusteredGridCells(Context &ctx, bool pass=0)
{
    glm::mat4 trackball = cg::cgTrackballGetRotationMatrix(ctx.interactor.trackball);
    glm::vec3 light_pos = glm::vec3(0.5f, 1.0f, 2.0f);
    glm::mat4 shadow = glm::perspective(glm::radians(35.0f), 1.0f, 0.5f, 3.0f) *
        glm::lookAt(light_pos, glm::vec3(0.0f), glm::vec3(0.0f, 1.0f, 0.0f)) * trackball;
    glm::mat4 shadow_from_view = shadow * glm::inverse(ctx.scene_uniforms.view);

    glBindBufferBase(GL_UNIFORM_BUFFER, 0, ctx.buffers["scene_uniforms"]);
    glBindBufferBase(GL_UNIFORM_BUFFER, 1, ctx.buffers["drawable_uniforms"]);
    glBindTextureUnit(0, ctx.textures["cells"]);
    glBindTextureUnit(1, ctx.textures["metacells"]);
    glBindTextureUnit(2, ctx.textures["depth"]);
    glBindTextureUnit(3, pass == 1 ? ctx.textures["visibility2"] : ctx.textures["visibility"]);
    glBindTextureUnit(4, ctx.textures["volume"]);
    glBindImageTexture(0, ctx.textures["shadowmap"], 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32UI);

    GLenum draw_buffers[] = { ctx.settings.use_ewa_pass ? GL_NONE : GL_COLOR_ATTACHMENT3, GL_COLOR_ATTACHMENT4 };
    glDrawBuffers(2, draw_buffers);
    glUsePipelineState(ctx.pipelines["draw_cells"]);
    {
        glUniform1f(2, ctx.settings.point_scale);
        glUniform1i(3, ctx.settings.shadows_enabled);
        glUniformMatrix4fv(4, 1, GL_FALSE, &shadow_from_view[0][0]);
        glViewport(0, 0, ctx.window.width, ctx.window.height);

        glUniform1i(0, 1);
        glPatchParameteri(GL_PATCH_VERTICES, 1);
        glDrawArrays(GL_PATCHES, 0, ctx.cgc.metacells.size() / 16);

        GLenum draw_buffers[] = { GL_NONE, GL_COLOR_ATTACHMENT4 };
        glDrawBuffers(2, draw_buffers);
        glDepthMask(GL_FALSE);
        glUniform1i(0, 0);
        glPatchParameteri(GL_PATCH_VERTICES, 1);
        if (pass != 1)
            glDrawArrays(GL_PATCHES, 0, ctx.cgc.metacells.size() / 16);
    }
    glClearCurrentPipelineState();

    if (ctx.settings.use_ewa_pass) {
        GLenum draw_buffers2[] = { GL_COLOR_ATTACHMENT3, GL_NONE };
        glDrawBuffers(2, draw_buffers2);
        glUsePipelineState(ctx.pipelines["draw_cells_ewa"]);
        {
            glUniform1f(2, ctx.settings.point_scale);
            glViewport(0, 0, ctx.window.width, ctx.window.height);

            glPatchParameteri(GL_PATCH_VERTICES, 1);
            glDrawArrays(GL_PATCHES, 0, ctx.cgc.metacells.size() / 16);
        }
        glClearCurrentPipelineState();
    }
    glDrawBuffer(GL_BACK);
}

void drawMesh(Context &ctx)
{
    uint32_t num_indices = uint32_t(ctx.mesh.indices.size());

    glm::mat4 trackball = cg::cgTrackballGetRotationMatrix(ctx.interactor.trackball);
    glm::vec3 light_pos = glm::vec3(0.5f, 1.0f, 2.0f);
    glm::mat4 shadow = glm::perspective(glm::radians(35.0f), 1.0f, 0.5f, 3.0f) *
        glm::lookAt(light_pos, glm::vec3(0.0f), glm::vec3(0.0f, 1.0f, 0.0f)) * trackball;
    glm::mat4 shadow_from_view = shadow * glm::inverse(ctx.scene_uniforms.view);

    glBindBufferBase(GL_UNIFORM_BUFFER, 0, ctx.buffers["scene_uniforms"]);
    glBindBufferBase(GL_UNIFORM_BUFFER, 1, ctx.buffers["drawable_uniforms"]);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ctx.buffers["mesh_indices"]);
    glBindTextureUnit(0, ctx.textures["mesh"]);
    glBindImageTexture(0, ctx.textures["shadowmap"], 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32UI);

    GLenum draw_buffers[] = { GL_COLOR_ATTACHMENT3, GL_COLOR_ATTACHMENT4 };
    glDrawBuffers(2, draw_buffers);
    glUsePipelineState(ctx.pipelines["draw_mesh"]);
    {
        glUniform1f(0, ctx.settings.point_scale);
        glUniform1i(1, ctx.settings.shadows_enabled);
        glUniformMatrix4fv(2, 1, GL_FALSE, &shadow_from_view[0][0]);
        glViewport(0, 0, ctx.window.width, ctx.window.height);
        glDrawElements(GL_TRIANGLES, num_indices, GL_UNSIGNED_INT, 0);
        glMemoryBarrier(GL_ALL_BARRIER_BITS);
    }
    glDrawBuffer(GL_BACK);
    glClearCurrentPipelineState();
}

void drawGrid(Context &ctx)
{
    glBindBufferBase(GL_UNIFORM_BUFFER, 0, ctx.buffers["scene_uniforms"]);
    glBindBufferBase(GL_UNIFORM_BUFFER, 1, ctx.buffers["drawable_uniforms"]);
    glBindTextureUnit(0, ctx.textures["blocks"]);
    glBindTextureUnit(1, ctx.textures["metacells"]);

    GLenum draw_buffers[] = { GL_COLOR_ATTACHMENT0, GL_NONE };
    glDrawBuffers(2, draw_buffers);

    glUsePipelineState(ctx.pipelines["draw_grid"]);
    {
        glViewport(0, 0, ctx.window.width, ctx.window.height);
        glUniform3iv(1, 1, &ctx.volume.base.dimensions[0]);
        glUniform1i(0, 0 /* OP_DRAW_BLOCKS */);
        glDrawArrays(GL_POINTS, 0, 4096);
        glUniform1i(0, 1 /* OP_DRAW_CLUSTERS */);
        glDrawArrays(GL_POINTS, 0, ctx.cgc.metacells.size() / 16);
    }
    glClearCurrentPipelineState();
}

void drawFxaaGamma(Context &ctx)
{
    glBindTextureUnit(0, ctx.textures["color"]);

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glDrawBuffer(GL_BACK);
    glUsePipelineState(ctx.pipelines["fxaa_gamma"]);
    {
        glViewport(0, 0, ctx.window.width, ctx.window.height);
        glUniform1i(0, ctx.settings.fxaa_enabled);
        glUniform1i(1, ctx.settings.gamma_enabled);
        glDrawArrays(GL_TRIANGLES, 0, 3);
    }
    glClearCurrentPipelineState();
}

void dispatchRaytrace(Context &ctx)
{
    glBindBufferBase(GL_UNIFORM_BUFFER, 0, ctx.buffers["scene_uniforms"]);
    glBindBufferBase(GL_UNIFORM_BUFFER, 1, ctx.buffers["drawable_uniforms"]);
    glBindTextureUnit(0, ctx.textures["cells"]);
    glBindTextureUnit(1, ctx.textures["metacells"]);
    glBindTextureUnit(2, ctx.textures["blocks"]);
    glBindTextureUnit(3, ctx.textures["bluenoise"]);
    glBindTextureUnit(4, ctx.textures["envmap"]);
    glBindTextureUnit(5, ctx.textures["depth"]);
    glBindImageTexture(1, ctx.textures["gbuffer"], 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA16F);
    glBindImageTexture(2, ctx.textures["color"], 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA16F);

    glUseProgram(ctx.programs["raytrace_cs"]);
    {
        glUniform3iv(0, 1, &ctx.volume.base.dimensions[0]);
        glUniform1i(1, ctx.settings.material);
        glUniform1i(2, ctx.settings.show_envmap);
        glDispatchCompute(ctx.window.width / 8, ctx.window.height / 8, 1);
        glMemoryBarrier(GL_ALL_BARRIER_BITS);
    }
    glUseProgram(0);
}

void dispatchRaytraceCPU(Context &ctx)
{
    // Collect scene state
    const auto *blocks = &ctx.cgc.blocks[0][0];
    const auto *metacells = &ctx.cgc.metacells[0][0];
    auto scene_uniforms = ctx.scene_uniforms;
    auto world_from_view = glm::inverse(scene_uniforms.view);

    // Allocate an RGBA32F framebuffer
    const auto w = ctx.window.width;
    const auto h = ctx.window.height;
    std::vector<float> color(w * h * 4);

    // Do ray tracing of scene (i.e., grid cells)
    #pragma omp parallel for schedule(dynamic, 4)
    for (int y = 0; y < h; ++y) {
        int viewport[] = { 0, y, w, 1 };
        ispc::raytrace_ispc((ispc::SceneUniforms &)scene_uniforms, &world_from_view[0][0],
                blocks, metacells, viewport, &color[0], nullptr);
    }

    // Update OpenGL color texture to display result
    glTextureSubImage2D(ctx.textures["color"], 0, 0, 0, w, h, GL_RGBA, GL_FLOAT, &color[0]);
}

void doRendering(Context &ctx)
{
    if (ctx.window.idle_frames < 100 || ctx.window.update_when_idle) {
        // Clear timers
        for (auto &ti : ctx.timers) { ti.second.result = 0; }

        // Update per-frame uniforms
        glNamedBufferSubData(ctx.buffers["scene_uniforms"], 0,
                sizeof(ctx.scene_uniforms), &ctx.scene_uniforms);
        glNamedBufferSubData(ctx.buffers["drawable_uniforms"], 0,
                sizeof(ctx.drawable_uniforms), &ctx.drawable_uniforms);

        // Clear framebuffers/textures
        glClearTexImage(ctx.textures["shadowmap"], 0, GL_RED, GL_FLOAT, &glm::vec4(1.0f)[0]);
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, ctx.framebuffers["forward"]);
        glDrawBuffer(GL_COLOR_ATTACHMENT4);  // ID texture
        glClearBufferfv(GL_COLOR, 0, &glm::vec4(0.0f)[0]);
        glDrawBuffer(GL_COLOR_ATTACHMENT3);  // G-buffer
        glClearBufferfv(GL_COLOR, 0, &glm::vec4(0.0f)[0]);
        glDrawBuffer(GL_COLOR_ATTACHMENT2);  // AO
        glClearBufferfv(GL_COLOR, 0, &glm::vec4(1.0f)[0]);
        glDrawBuffer(GL_COLOR_ATTACHMENT0);  // Color
        glClearBufferfv(GL_COLOR, 0, &glm::vec4(0.0f)[0]);
        glClearBufferfi(GL_DEPTH_STENCIL, 0, 1.0f, 0);

        if (ctx.settings.show_cells) {
            glBeginQuery(GL_TIME_ELAPSED, ctx.timers["rasterise"].query);
            drawClusteredGridCells(ctx, 0);
            glEndQuery(GL_TIME_ELAPSED);
            glGetQueryObjectui64v(ctx.timers["rasterise"].query, GL_QUERY_RESULT,
                    &ctx.timers["rasterise"].result);

            if (!ctx.settings.freeze_visibility) {
                glBeginQuery(GL_TIME_ELAPSED, ctx.timers["visibility"].query);
                dispatchVisibility(ctx);
                drawClusteredGridCells(ctx, 1);
                glEndQuery(GL_TIME_ELAPSED);
                glGetQueryObjectui64v(ctx.timers["visibility"].query, GL_QUERY_RESULT,
                        &ctx.timers["visibility"].result);
            }
        }

        if (ctx.settings.show_mesh) {
            glBeginQuery(GL_TIME_ELAPSED, ctx.timers["rasterise"].query);
            drawMesh(ctx);
            glEndQuery(GL_TIME_ELAPSED);
            glGetQueryObjectui64v(ctx.timers["rasterise"].query, GL_QUERY_RESULT,
                    &ctx.timers["rasterise"].result);
        }

        if (ctx.settings.ssao_enabled) {
            glBeginQuery(GL_TIME_ELAPSED, ctx.timers["ssao"].query);
            dispatchSSAO(ctx);
            glEndQuery(GL_TIME_ELAPSED);
            glGetQueryObjectui64v(ctx.timers["ssao"].query, GL_QUERY_RESULT,
                    &ctx.timers["ssao"].result);
        }

        glBeginQuery(GL_TIME_ELAPSED, ctx.timers["shadows"].query);
        dispatchFixShadows(ctx);
        glEndQuery(GL_TIME_ELAPSED);
        glGetQueryObjectui64v(ctx.timers["shadows"].query, GL_QUERY_RESULT,
                &ctx.timers["shadows"].result);

        glBeginQuery(GL_TIME_ELAPSED, ctx.timers["deferred"].query);
        dispatchDeferred(ctx);
        glEndQuery(GL_TIME_ELAPSED);
        glGetQueryObjectui64v(ctx.timers["deferred"].query, GL_QUERY_RESULT,
                &ctx.timers["deferred"].result);

        if (ctx.settings.raytracing_enabled) {
            if (ctx.settings.raytracing_mode == 0) {
                glBeginQuery(GL_TIME_ELAPSED, ctx.timers["raytrace"].query);
                dispatchRaytrace(ctx);
                glEndQuery(GL_TIME_ELAPSED);
                glGetQueryObjectui64v(ctx.timers["raytrace"].query, GL_QUERY_RESULT,
                        &ctx.timers["raytrace"].result);
            }
            if (ctx.settings.raytracing_mode == 1) {
                double tic = glfwGetTime();
                dispatchRaytraceCPU(ctx);
                ctx.timers["raytrace"].result = (glfwGetTime() - tic) * 1e3;
            }
        }

        if (ctx.settings.show_grid) {
            drawGrid(ctx);
        }
    }

    glBeginQuery(GL_TIME_ELAPSED, ctx.timers["fxaa_gamma"].query);
    drawFxaaGamma(ctx);
    glEndQuery(GL_TIME_ELAPSED);
    glGetQueryObjectui64v(ctx.timers["fxaa_gamma"].query, GL_QUERY_RESULT,
                          &ctx.timers["fxaa_gamma"].result);
}

void showSettingsWindow(Context &ctx)
{
    ImGui::SetNextWindowCollapsed(true, ImGuiSetCond_Once);
    if (ImGui::Begin("Settings")) {
        if (ImGui::CollapsingHeader("Rasterisation")) {
            ImGui::Checkbox("Show cells", (bool *)&ctx.settings.show_cells);
            ImGui::Checkbox("Show normals", (bool *)&ctx.settings.show_normals);
            ImGui::Checkbox("Show cluster ID", (bool *)&ctx.settings.show_cluster_id);
            ImGui::Checkbox("Show grid", (bool *)&ctx.settings.show_grid);
        #if EXTRACT_REFERENCE_MESH
            ImGui::Checkbox("Show mesh", (bool *)&ctx.settings.show_mesh);
        #endif
            ImGui::Checkbox("Use EWA pass", (bool *)&ctx.settings.use_ewa_pass);
            ImGui::Checkbox("Freeze visibility", (bool *)&ctx.settings.freeze_visibility);
            ImGui::Checkbox("SSAO enabled", (bool *)&ctx.settings.ssao_enabled);
            ImGui::Checkbox("Shadows enabled", (bool *)&ctx.settings.shadows_enabled);
            ImGui::SliderFloat("Point scale", &ctx.settings.point_scale, 0.1f, 3.0f, "%.1f");
        }
        if (ImGui::CollapsingHeader("Ray tracing")) {
            ImGui::Checkbox("Raytracing enabled", (bool *)&ctx.settings.raytracing_enabled);
            ImGui::Combo("Raytracing mode", &ctx.settings.raytracing_mode, "GPU\0CPU\0");
            ImGui::Combo("Material", &ctx.settings.material, "Matte\0Metal\0");
            ImGui::Checkbox("Show environment map", (bool *)&ctx.settings.show_envmap);
        }
        if (ImGui::CollapsingHeader("Misc")) {
            ImGui::ColorEdit3("Background color", &ctx.settings.background_color[0]);
            ImGui::ColorEdit3("Material albedo", &ctx.settings.material_albedo[0]);
            if (ImGui::BeginPopupContextItem("Palette picker")) {
                for (unsigned i = 0; i < 8; ++i) {
                    ImVec4 color = ImColor::HSV(0.618034f * i, 0.7, 0.8);
                    ImGui::PushID(i + 1234);
                    if (ImGui::ColorButton(color))
                        ctx.settings.material_albedo = (glm::vec3 &)color;
                    ImGui::PopID();
                    ImGui::SameLine();
                }
                ImGui::EndPopup();
            }
            ImGui::Checkbox("FXAA enabled", (bool *)&ctx.settings.fxaa_enabled);
            ImGui::Checkbox("Gamma enabled", (bool *)&ctx.settings.gamma_enabled);
            ImGui::Checkbox("Update when idle", (bool *)&ctx.window.update_when_idle);
            if (ImGui::Checkbox("Vsync enabled", (bool *)&ctx.window.swap_interval))
                glfwSwapInterval(ctx.window.swap_interval & 1);
        }
    }
    ImGui::End();
}

void showPerformanceWindow(Context &ctx)
{
    ImGui::SetNextWindowCollapsed(true, ImGuiSetCond_Once);
    if (ImGui::Begin("Performance")) {
        ImGui::Text("Cell count: %lu", ctx.cgc.cells.size());
        ImGui::Text("Metacell count: %lu", ctx.cgc.metacells.size());
        ImGui::Text("Cluster count: %lu", ctx.cgc.metacells.size() / CLUSTER_SIZE);
        ImGui::Text("Block count: %lu", ctx.cgc.blocks.size());
        ImGui::Text("Resolution: %dx%d", ctx.window.width, ctx.window.height);
        ImGui::Text("Frame rate: %.1f", ImGui::GetIO().Framerate);
        ImGui::Text("Frame times (GPU):");
        {
            int i = 0; double t0 = 0.0, t1 = 0.0;
            for (auto &timer : ctx.timers) {
                t0 += (t1 = (double(timer.second.result) / 1e6));
                ImGui::PushStyleColor(ImGuiCol_Button, ImColor::HSV((i++) / 7.0f, 0.6f, 0.6f));
                ImGui::Button("", ImVec2(float(t1 * (ImGui::GetWindowWidth() / 16.7)), 8));
                if (ImGui::IsItemHovered()) { ImGui::SetTooltip("%s: %.1lf (ms)", timer.first.c_str(), t1); }
                ImGui::PopStyleColor();
            }
            ImGui::PushStyleColor(ImGuiCol_Button, ImColor::HSV(i / 7.0f, 0.6f, 0.6f));
            ImGui::Button("", ImVec2(float(t0 * (ImGui::GetWindowWidth() / 16.7)), 8));
            if (ImGui::IsItemHovered()) { ImGui::SetTooltip("total: %.1lf (ms)", t0); }
            ImGui::PopStyleColor();
        }
        int i = 0;
        for (auto &timer : ctx.timers) {
            ImGui::TextColored(ImColor::HSV((i++) / 7.0f, 0.6f, 0.6f), "%s", timer.first.c_str());
            ImGui::SameLine();
        }
        ImGui::TextColored(ImColor::HSV(i / 7.0f, 0.6f, 0.6f), "total");
    }
    ImGui::End();
}

void doUpdate(Context &ctx)
{
    if (ctx.window.idle_frames < 100 || ctx.window.update_when_idle) {
        // Update viewing
        glm::mat4 trackball = cg::cgTrackballGetRotationMatrix(ctx.interactor.trackball);
        ctx.camera.view = glm::translate(glm::mat4(), -ctx.camera.location);
        ctx.camera.view = ctx.camera.view * trackball;

        // Update per-frame uniforms
        ctx.scene_uniforms.projection = ctx.camera.projection;
        ctx.scene_uniforms.view = ctx.camera.view;
        ctx.scene_uniforms.resolution[0] = float(ctx.window.width);
        ctx.scene_uniforms.resolution[1] = float(ctx.window.height);
        ctx.scene_uniforms.time = ctx.window.elapsed_time;
        ctx.scene_uniforms.frame += 1;
    }

    // Show GUI
    showSettingsWindow(ctx);
    showPerformanceWindow(ctx);
}

void errorCallback(int /*error*/, const char* description)
{
    std::fprintf(stderr, "%s\n", description);
}

void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
    ImGui_ImplGlfwGL3_MouseButtonCallback(window, button, action, mods);
    if (ImGui::GetIO().WantCaptureMouse) { return; }  // Skip other handling

    double x, y;
    glfwGetCursorPos(window, &x, &y);

    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));
    ctx->window.idle_frames = 0;
    if (button == GLFW_MOUSE_BUTTON_RIGHT) {
        if (action == GLFW_PRESS) {
            ctx->interactor.trackball.center = { x, y };
            cg::cgTrackballStartTracking(ctx->interactor.trackball, { x, y });
        }
        if (action == GLFW_RELEASE) {
            cg::cgTrackballStopTracking(ctx->interactor.trackball);
        }
    }
    if (button == GLFW_MOUSE_BUTTON_MIDDLE) {
        ctx->interactor.panning = (action == GLFW_PRESS);
        ctx->interactor.pan_center = { x, y };
    }
}

void scrollCallback(GLFWwindow* window, double /*xoffset*/, double yoffset)
{
    if (ImGui::GetIO().WantCaptureMouse) { return; }  // Skip other handling

    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));
    ctx->window.idle_frames = 0;
    ctx->camera.location.z += 0.05f * glm::sign(float(yoffset));
    ctx->camera.location.z = glm::clamp(ctx->camera.location.z, 0.5f, 4.0f);
}

void cursorPosCallback(GLFWwindow* window, double x, double y)
{
    if (ImGui::GetIO().WantCaptureMouse) { return; }  // Skip other handling

    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));
    if (ctx->interactor.trackball.tracking) {
        cg::cgTrackballMove(ctx->interactor.trackball, { x, y });
        ctx->window.idle_frames = 0;
    }
    if (ctx->interactor.panning) {
        ctx->camera.location.x -= (float(x) - ctx->interactor.pan_center.x) * 0.001f;
        ctx->camera.location.y += (float(y) - ctx->interactor.pan_center.y) * 0.001f;
        ctx->interactor.pan_center = { x, y };
        ctx->window.idle_frames = 0;
    }
}

void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    ImGui_ImplGlfwGL3_KeyCallback(window, key, scancode, action, mods);
    if (ImGui::GetIO().WantCaptureKeyboard) { return; }  // Skip other handling

    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));
    if (key == GLFW_KEY_F5 && action == GLFW_PRESS) {
        setupGLPrograms(*ctx);
        setupGLPipelines(*ctx);
        ctx->window.idle_frames = 0;
    }
}

void resizeCallback(GLFWwindow* window, int width, int height)
{
    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));
    ctx->window.width = width;
    ctx->window.height = height;
    ctx->window.aspect = float(width) / float(height);
    ctx->window.idle_frames = 0;

    setupGLFramebuffers(*ctx);
    setupTrackball(*ctx);
    setupCameras(*ctx);
}

int main(int argc, char *argv[])
{
    // Create application context (for passing around)
    Context ctx;
    if (argc > 1) {
        ctx.settings.input_filename = std::string(argv[1]);
        if (argc > 2) {
            ctx.settings.input_scale = std::atoi(argv[2]);
        }
    }

    // Create window and GL context
    glfwSetErrorCallback(errorCallback);
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    ctx.window.id = glfwCreateWindow(ctx.window.width, ctx.window.height, "Clustered Grid Cells", 0, 0);
    if (!ctx.window.id) {
        std::exit(EXIT_FAILURE);
    }
    glfwMakeContextCurrent(ctx.window.id);
    glfwSetWindowUserPointer(ctx.window.id, &ctx);
    glfwSetMouseButtonCallback(ctx.window.id, mouseButtonCallback);
    glfwSetScrollCallback(ctx.window.id, scrollCallback);
    glfwSetCursorPosCallback(ctx.window.id, cursorPosCallback);
    glfwSetKeyCallback(ctx.window.id, keyCallback);
    glfwSetFramebufferSizeCallback(ctx.window.id, resizeCallback);
    glfwSwapInterval(ctx.window.swap_interval);

    // Load GL functions
    glewExperimental = true;
    GLenum glew_status = glewInit();
    if (GLEW_OK != glew_status) {
        std::fprintf(stderr, "%s\n", glewGetErrorString(glew_status));
        std::exit(EXIT_FAILURE);
    }
    std::fprintf(stdout, "GL version: %s\n", glGetString(GL_VERSION));

    // Create default VAO and bind it once here
    glCreateVertexArrays(1, &ctx.vaos["default"]);
    glBindVertexArray(ctx.vaos["default"]);

    // Initialize GUI without installing default callbacks
    ImGui_ImplGlfwGL3_Init(ctx.window.id, false);

    // Initialize application
    doInitialization(ctx);

    // Start main loop
    while (!glfwWindowShouldClose(ctx.window.id)) {
        glfwPollEvents();
        ctx.window.elapsed_time = float(glfwGetTime());
        ctx.window.idle_frames += 1;
        ImGui_ImplGlfwGL3_NewFrame();
        doUpdate(ctx);
        doRendering(ctx);
        ImGui::Render();
        glfwSwapBuffers(ctx.window.id);
    }

    // Shutdown everything
    glfwDestroyWindow(ctx.window.id);
    glfwTerminate();
    std::exit(EXIT_SUCCESS);
}
