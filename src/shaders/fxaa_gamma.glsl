#if defined(VERTEX)

layout(location = 0) out vec2 v_texcoord;

void main()
{
    // Generate vertices for fullscreen triangle
    v_texcoord = vec2(gl_VertexID % 2, gl_VertexID / 2) * 2.0;
    gl_Position = vec4(v_texcoord * 2.0 - 1.0, 0.0, 1.0);
}

#elif defined(FRAGMENT)

layout(location = 0) uniform int u_fxaa_enabled;
layout(location = 1) uniform int u_gamma_enabled;
layout(binding = 0) uniform sampler2D u_color_texture;

layout(location = 0) in vec2 v_texcoord;
layout(location = 0) out vec4 rt_color;

float rgb2luma(vec3 color)
{
    return dot(color, vec3(0.299, 0.587, 0.114));
}

vec3 linearToGamma(vec3 color)
{
    return pow(color, vec3(1.0 / 2.2));
}

// Adaption of Timothy Lotte's original FXAA implementation
vec3 fxaa(sampler2D tex, vec2 uv)
{
    const float REDUCE_MIN = 1.0 / 128.0;
    const float REDUCE_MUL = 1.0 / 8.0;
    const float SPAN_MAX = 8.0;

    vec2 uv_shifted = uv - (0.5 / textureSize(tex, 0).xy);
    float lumaNW = rgb2luma(textureLodOffset(tex, uv_shifted, 0, ivec2(0, 0)).rgb);
    float lumaNE = rgb2luma(textureLodOffset(tex, uv_shifted, 0, ivec2(1, 0)).rgb);
    float lumaSW = rgb2luma(textureLodOffset(tex, uv_shifted, 0, ivec2(0, 1)).rgb);
    float lumaSE = rgb2luma(textureLodOffset(tex, uv_shifted, 0, ivec2(1, 1)).rgb);
    float lumaM =  rgb2luma(textureLod(tex, uv, 0).rgb);

    float lumaMin = min(lumaM, min(min(lumaNW, lumaNE), min(lumaSW, lumaSE)));
    float lumaMax = max(lumaM, max(max(lumaNW, lumaNE), max(lumaSW, lumaSE)));

    vec2 dir;
    dir.x = -((lumaNW + lumaNE) - (lumaSW + lumaSE));
    dir.y =  ((lumaNW + lumaSW) - (lumaNE + lumaSE));

    float dirReduce = max(REDUCE_MIN, (lumaNW + lumaNE + lumaSW + lumaSE) * 0.25 * REDUCE_MUL);
    float rcpDirMin = 1.0 / (min(abs(dir.x), abs(dir.y)) + dirReduce);
    dir = clamp(dir * rcpDirMin, -SPAN_MAX, SPAN_MAX) / textureSize(tex, 0).xy;

    vec3 rgbA = 0.5 * (
        textureLod(tex, uv + dir * (1.0 / 3.0 - 0.5), 0).rgb +
        textureLod(tex, uv + dir * (2.0 / 3.0 - 0.5), 0).rgb);
    vec3 rgbB = rgbA * 0.5 + 0.25 * (
        textureLod(tex, uv + dir * (0.0 / 3.0 - 0.5), 0).rgb +
        textureLod(tex, uv + dir * (3.0 / 3.0 - 0.5), 0).rgb);
    float lumaB = rgb2luma(rgbB);
    return ((lumaB < lumaMin) || (lumaB > lumaMax)) ? rgbA : rgbB;
}

void main()
{
    if (bool(u_fxaa_enabled))
        rt_color = vec4(fxaa(u_color_texture, v_texcoord), 1.0);
    else
        rt_color = texelFetch(u_color_texture, ivec2(gl_FragCoord.xy), 0);

    if (bool(u_gamma_enabled))
        rt_color.rgb = linearToGamma(rt_color.rgb);
}

#endif
