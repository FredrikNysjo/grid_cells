#define CLUSTER_SIZE 16
#define BLOCKS_PER_SIDE 16

#define OP_DRAW_BLOCKS 0
#define OP_DRAW_CLUSTERS 1

layout(binding = 0, std140) uniform SceneUniforms {
    mat4 view_from_world;
    mat4 proj_from_view;
    vec2 res;
    float time;
    int frame;
} u_scene;

layout(binding = 1, std140) uniform DrawableUniforms {
    mat4 world_from_local;
    mat4 normal_from_local;
} u_drawable;

layout(location = 0) uniform int u_op;
layout(location = 1) uniform ivec3 u_volume_dim = ivec3(1.0);
layout(binding = 0) uniform usamplerBuffer u_blocks;
layout(binding = 1) uniform usamplerBuffer u_metacells;

#if defined(VERTEX)

void main()
{
    // Pass-through
}

#elif defined(GEOMETRY)

layout(points) in;
layout(line_strip, max_vertices = 16) out;

const vec3 g_wirecube[] = vec3[](
    vec3(0, 0, 1), vec3(1, 0, 1), vec3(1, 0, 0), vec3(0, 0, 0),
    vec3(0, 0, 1), vec3(0, 1, 1), vec3(1, 1, 1), vec3(1, 0, 1),
    vec3(0, 1, 0), vec3(0, 1, 1), vec3(1, 1, 1), vec3(1, 1, 0),
    vec3(0, 1, 0), vec3(0, 0, 0), vec3(1, 0, 0), vec3(1, 1, 0)
);

void main()
{
    vec3 local_pos;
    vec3 local_scale;

    if (u_op == OP_DRAW_BLOCKS) {
        int block_id = gl_PrimitiveIDIn;
        uvec4 block = texelFetch(u_blocks, block_id);
        if (block.r == 0u) return;

        vec3 aspect = vec3(u_volume_dim) / max(u_volume_dim.x, max(u_volume_dim.y, u_volume_dim.z));
        local_pos.x = block_id % BLOCKS_PER_SIDE;
        local_pos.y = (block_id / BLOCKS_PER_SIDE) % BLOCKS_PER_SIDE;
        local_pos.z = (block_id / (BLOCKS_PER_SIDE * BLOCKS_PER_SIDE)) % BLOCKS_PER_SIDE;
        local_pos = aspect * (local_pos / BLOCKS_PER_SIDE - 0.5);
        local_scale = aspect / BLOCKS_PER_SIDE;
    }

    if (u_op == OP_DRAW_CLUSTERS) {
        int cluster_id = gl_PrimitiveIDIn;
        uvec3 cluster = texelFetch(u_metacells, cluster_id * CLUSTER_SIZE + (CLUSTER_SIZE - 1)).rgb;

        vec3 aabb_cluster[2];
        aabb_cluster[0] = vec3((cluster.xxy >> uvec3(0u, 16u, 0u)) & 65535u);
        aabb_cluster[1] = vec3((cluster.yzz >> uvec3(16u, 0u, 16u)) & 65535u) + 1.0;
        local_pos = vec3(u_drawable.world_from_local * vec4(aabb_cluster[0] * 2.0, 1.0));
        local_scale = vec3(u_drawable.world_from_local * vec4(aabb_cluster[1] * 2.0, 0.0));
    }

    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 8; ++j) {
            vec4 world_pos = vec4(local_pos + g_wirecube[i * 8 + j] * local_scale, 1.0);
            gl_Position = u_scene.proj_from_view * u_scene.view_from_world * world_pos;
            EmitVertex();
        }
        EndPrimitive();
    }
}

#elif defined(FRAGMENT)

layout(location = 0) out vec4 rt_color;

void main()
{
    if (u_op == OP_DRAW_BLOCKS)
        rt_color = vec4(0.0, 0.5, 1.0, 1.0) * 0.8;
    else
        rt_color = vec4(1.0, 0.0, 0.5, 1.0) * 0.8;
}

#endif
