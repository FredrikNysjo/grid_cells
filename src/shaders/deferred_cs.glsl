#define MAX_TEMPORAL_FRAMES 64
#define MAX_INTERPOLATION_RADIUS 32.0
#define NUM_NORMAL_SAMPLES 4
#define NUM_SHADOW_SAMPLES 4
#define SHADOW_BIAS 0.002
#define TWO_PI 6.283184

layout(binding = 0, std140) uniform SceneUniforms {
    mat4 view;
    mat4 projection;
    vec2 resolution;
    float time;
    int frame;
} u_scene;

layout(location = 0) uniform vec3 u_background_color;
layout(location = 1) uniform vec3 u_material_albedo;
layout(location = 2) uniform int u_show_normals;
layout(location = 3) uniform int u_show_cluster_id;
layout(location = 4) uniform mat4 u_shadow_from_view;
layout(binding = 0) uniform sampler2D u_depth;
layout(binding = 1, rgba16f) restrict uniform image2D u_gbuffer;
layout(binding = 2, rg32ui) restrict readonly uniform uimage2D u_id;
layout(binding = 3, r8) restrict readonly uniform image2D u_ao;
layout(binding = 4, r32ui) restrict readonly uniform uimage2D u_shadowmap;
layout(binding = 5, rgba16f) restrict writeonly uniform image2D u_color;

vec3 lds_r3(float n)
{
    float phi = 1.2207440846;
    return fract(n / vec3(phi, phi * phi, phi * phi * phi));
}

float shadowmapVisibility(vec3 view_pos, vec2 frag_pos)
{
    vec4 shadow_pos = u_shadow_from_view * vec4(view_pos, 1.0);
    vec3 ndc_pos = shadow_pos.xyz / shadow_pos.w;
    vec2 p = (ndc_pos.xy * 0.5 + 0.5) * vec2(512, 512);
    float depth = ndc_pos.z * 0.5 + 0.5;

    int frame = u_scene.frame % 64;
    float rnd = fract(dot(vec2(0.754877, 0.569840), frag_pos));
    float bias = SHADOW_BIAS * (fract(rnd + sqrt(3.0) * frame) + 2.0);

    float visibility = 0.0;
    for (int i = 0; i < NUM_SHADOW_SAMPLES; ++i) {
        float r = (i + 1.0) / NUM_SHADOW_SAMPLES;
        float theta = fract(rnd + 1.618034 * (i + NUM_SHADOW_SAMPLES * frame));
        vec2 disk = 1.5 * r * vec2(sin(theta * TWO_PI), cos(theta * TWO_PI));

        float texel = uintBitsToFloat(imageLoad(u_shadowmap, ivec2(p + 2.0 * disk))).r;
        visibility += float(texel > (depth - bias));
    }
    visibility *= 1.0 / NUM_SHADOW_SAMPLES;

    return visibility;
}

vec3 reconstructViewPos(vec3 ndc_pos, mat4 projection)
{
    float a = projection[0][0];
    float b = projection[1][1];
    float c = projection[2][2];
    float d = projection[3][2];
    float e = projection[2][0];
    float f = projection[2][1];
    float view_pos_z = -d / (ndc_pos.z + c);
    float view_pos_x = (-view_pos_z / a) * (ndc_pos.x + e);
    float view_pos_y = (-view_pos_z / b) * (ndc_pos.y + f);
    return vec3(view_pos_x, view_pos_y, view_pos_z);
}

vec3 computeLighting(vec3 view_normal, vec3 view_pos, vec3 albedo, float gloss, vec2 frag_pos)
{
    float ao = imageLoad(u_ao, ivec2(frag_pos)).r;
    float shadow = shadowmapVisibility(view_pos.xyz, frag_pos);

    vec3 N = normalize(view_normal);
    vec3 V = -normalize(view_pos);
    vec3 L = normalize(vec3(0.5, 1.0, 2.0) - view_pos);
    vec3 H = normalize(V + L);
    float N_dot_L = max(0.0, dot(N, L));
    float N_dot_H = max(0.0, dot(N, H));
    float alpha = exp2(10.0 * gloss);
    float normalization = (8.0 + alpha) / 8.0;
    vec3 F0 = vec3(0.04);
    vec3 F_schlick = (F0 + (1.0 - F0) * pow(1.0 - max(0.0, dot(N, V)), 5.0));
    albedo *= (1.0 - F0);

    vec3 color = vec3(0.0);
    color += albedo * 0.8 * shadow * N_dot_L;
    color += F0 * normalization * 0.8 * shadow * pow(N_dot_H, alpha);
    color += albedo * 0.4 * ao * (N.y * 0.5 + 0.5);
    color += F_schlick * 0.4 * ao * clamp(reflect(N, -V).y / (1.0 - gloss) + 0.5, 0.0, 1.0);
    return color;
}

#if defined(COMPUTE)

layout(local_size_x = 8, local_size_y = 8) in;

void main()
{
    vec2 frag_pos = vec2(gl_WorkGroupID.xy * gl_WorkGroupSize.xy + gl_LocalInvocationID.xy) + 0.5;
    int frame = u_scene.frame % MAX_TEMPORAL_FRAMES;

    float frag_depth = texelFetch(u_depth, ivec2(frag_pos), 0).r;
    if (frag_depth >= 1.0) {
        imageStore(u_color, ivec2(frag_pos), vec4(u_background_color, 1.0));
        return;
    }

    vec4 texel = imageLoad(u_gbuffer, ivec2(frag_pos));
    vec3 view_normal = normalize(texel.rgb);
    vec3 albedo = u_material_albedo * u_material_albedo;
    float gloss = 0.7;
    if (bool(u_show_cluster_id)) {
        int cluster_id = int(imageLoad(u_id, ivec2(frag_pos)).r >> 1) - 1;
        albedo = lds_r3((cluster_id & 1023) + 0.5);
        gloss = 0.4;
    }

    vec3 ndc_pos = vec3(frag_pos / u_scene.resolution.xy, frag_depth) * 2.0 - 1.0;
    vec3 view_pos = reconstructViewPos(ndc_pos, u_scene.projection);

    vec4 output_color = vec4(1.0);
    output_color.rgb = computeLighting(view_normal, view_pos, albedo, gloss, frag_pos);

    if (bool(u_show_normals))
        output_color.rgb = view_normal * 0.5 + 0.5;

    imageStore(u_color, ivec2(frag_pos), output_color);
}

#endif
