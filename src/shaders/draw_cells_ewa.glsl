#define META_SIZE 8
#define CLUSTER_SIZE 16
#define MAX_CLUSTERS (1 << 20)

#define DISCARD_VERTEX { gl_Position = vec4(1.0, 1.0, 1.0, 0.0); return; }

layout(binding = 0, std140) uniform SceneUniforms {
    mat4 view;
    mat4 proj;
    vec2 res;
    float time;
    int frame;
} u_scene;

layout(binding = 1, std140) uniform DrawableUniforms {
    mat4 model;
    mat4 normal;
} u_drawable;

layout(location = 0) uniform int u_level = 1;
layout(location = 2) uniform float u_point_scale;
layout(binding = 0) uniform samplerBuffer u_cells;
layout(binding = 1) uniform usamplerBuffer u_metacells;
layout(binding = 2) uniform sampler2D u_depth;
layout(binding = 3) uniform usamplerBuffer u_visibility;

vec3 cellGrad(vec4 c0, vec4 c1)
{
    float grad_x = (c0[1] + c0[3] + c1[1] + c1[3]) - (c0[0] + c0[2] + c1[0] + c1[2]);
    float grad_y = (c0[2] + c0[3] + c1[2] + c1[3]) - (c0[0] + c0[1] + c1[0] + c1[1]);
    float grad_z = (c1[0] + c1[1] + c1[2] + c1[3]) - (c0[0] + c0[1] + c0[2] + c0[3]);
    return vec3(grad_x, grad_y, grad_z) * 0.25;
}

#if defined(VERTEX)

void main()
{
    // Pass-through
}

#elif defined(TESS_CONTROL)

layout(vertices = 1) out;

void main()
{
    int cluster_id = gl_PrimitiveID;
    int cluster_bit = cluster_id & 31;
    uint visibility = texelFetch(u_visibility, cluster_id >> 5).r & (1u << cluster_bit);

    gl_TessLevelOuter[1] = (u_level > 0)
                         ? ((visibility > 0) ? 15 : 0)
                         : ((visibility > 0) ? 0 : 15);
    gl_TessLevelOuter[0] = u_level > 0 ? 8 : 1;
}

#elif defined(TESS_EVALUATION)

layout(isolines, point_mode) in;

layout(location = 0) flat out vec3 v_normal;
layout(location = 1) flat out float v_max_depth;

void main()
{
    int idx2 = int(round(gl_TessCoord.x * gl_TessLevelOuter[1]));
    int idx = int(round(gl_TessCoord.y * gl_TessLevelOuter[0]));

    uvec3 metacell = texelFetch(u_metacells, gl_PrimitiveID * CLUSTER_SIZE + idx2).rgb;
    vec3 local_pos = vec3((metacell.xxy >> uvec3(0u, 16u, 0u)) & 65535u) * 2.0 + 0.5;
    uint mask = (metacell.y >> 16u) & 255u;
    int cell_id = u_level > 0 ? idx : (gl_PrimitiveID + u_scene.frame) % META_SIZE;
    int cell_offset = int(metacell.z + bitCount(mask & ~(0xffu << cell_id)));

    if ((mask & (1u << cell_id)) == 0u || idx2 == 15) DISCARD_VERTEX;

    vec4 c0 = (u_level > 0) ? texelFetch(u_cells, cell_offset * 2 + 0) : vec4(0.0);
    vec4 c1 = (u_level > 0) ? texelFetch(u_cells, cell_offset * 2 + 1) : vec4(0.0);
    vec3 local_normal = -normalize(cellGrad(c0, c1));
    local_pos += vec3((uvec3(cell_id) >> uvec3(0u, 1u, 2u)) & 1u);

    vec4 view_pos = u_scene.view * (u_drawable.model * vec4(local_pos, 1.0));
    vec3 view_normal = normalize(mat3(u_scene.view) * local_normal);
    float view_radius = 0.5 * length(u_drawable.model[0].xyz);
    bool front_facing = u_level == 0 || dot(normalize(view_pos.xyz), view_normal) < 0.01;
    bool visible = front_facing && view_pos.z < 0.0;
    if (!visible) DISCARD_VERTEX;

    gl_PointSize = (-view_radius / view_pos.z) * u_scene.proj[1][1] * u_scene.res[1];
    gl_PointSize *= u_point_scale * 1.3;
    vec4 clip_pos = u_scene.proj * vec4(view_pos.xyz + normalize(view_pos.xyz) * view_radius * 4.0, 1.0);
    view_pos.xyz += -normalize(view_pos.xyz) * view_radius * 4.0;
    gl_Position = u_scene.proj * view_pos;

    v_normal = view_normal;
    v_max_depth = (clip_pos.z / clip_pos.w) * 0.5 + 0.5;
}

#elif defined(FRAGMENT)

layout(location = 0) flat in vec3 v_normal;
layout(location = 1) flat in float v_max_depth;
layout(location = 0) out vec4 rt_gbuffer;

void main()
{
    float fade = max(0.1, 1.0 - length(gl_PointCoord - 0.5) * 2.0);
    if (texelFetch(u_depth, ivec2(gl_FragCoord.xy), 0).r > v_max_depth) fade = 0.0;
    rt_gbuffer = vec4(v_normal, 1.0) * fade;
}

#endif
