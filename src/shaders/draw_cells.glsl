#define META_SIZE 8
#define CLUSTER_SIZE 16
#define MAX_CLUSTERS (1 << 20)
#define USE_VOXEL_RASTERISATION 1
#define USE_CELL_RASTERISATION 1
#define USE_VOLUME_SAMPLING 0

#define DISCARD_VERTEX { gl_Position = vec4(1.0, 1.0, 1.0, 0.0); return; }

layout(binding = 0, std140) uniform SceneUniforms {
    mat4 view;
    mat4 proj;
    vec2 res;
    float time;
    int frame;
} u_scene;

layout(binding = 1, std140) uniform DrawableUniforms {
    mat4 model;
    mat4 normal;
} u_drawable;

layout(location = 0) uniform int u_level;
layout(location = 2) uniform float u_point_scale;
layout(location = 3) uniform int u_shadows_enabled;
layout(location = 4) uniform mat4 u_shadow_from_view;
layout(binding = 0) uniform samplerBuffer u_cells;
layout(binding = 1) uniform usamplerBuffer u_metacells;
layout(binding = 3) uniform usamplerBuffer u_visibility;
layout(binding = 4) uniform sampler3D u_volume;
layout(binding = 0, r32ui) uniform restrict uimage2D u_shadowmap;

vec3 lds_r3(float n)
{
    float phi = 1.2207440846;
    return fract(n / vec3(phi, phi * phi, phi * phi * phi));
}

float cellLinear(vec3 p, vec4 c0, vec4 c1)
{
    return mix(mix(mix(c0[0], c0[1], p.x), mix(c0[2], c0[3], p.x), p.y),
               mix(mix(c1[0], c1[1], p.x), mix(c1[2], c1[3], p.x), p.y), p.z);
}

vec3 cellGrad(vec4 c0, vec4 c1)
{
    float grad_x = (c0[1] + c0[3] + c1[1] + c1[3]) - (c0[0] + c0[2] + c1[0] + c1[2]);
    float grad_y = (c0[2] + c0[3] + c1[2] + c1[3]) - (c0[0] + c0[1] + c1[0] + c1[1]);
    float grad_z = (c1[0] + c1[1] + c1[2] + c1[3]) - (c0[0] + c0[1] + c0[2] + c0[3]);
    return vec3(grad_x, grad_y, grad_z) * 0.25;
}

void splatShadowDepth(vec3 local_pos)
{
    vec4 clip_pos = u_shadow_from_view * (u_scene.view * (u_drawable.model * vec4(local_pos, 1.0)));
    vec3 ndc_pos = clip_pos.xyz / clip_pos.w;
    vec2 frag_pos = (ndc_pos.xy * 0.5 + 0.5) * vec2(512, 512);
    float frag_depth = ndc_pos.z * 0.5 + 0.5;
    imageAtomicMin(u_shadowmap, ivec2(frag_pos), floatBitsToUint(frag_depth));
}

#if defined(VERTEX)

void main()
{
    // Pass-through
}

#elif defined(TESS_CONTROL)

layout(vertices = 1) out;

void main()
{
    int cluster_id = gl_PrimitiveID;
    int cluster_bit = cluster_id & 31;
    uint visibility = texelFetch(u_visibility, cluster_id >> 5).r & (1u << cluster_bit);

    gl_TessLevelOuter[1] = (u_level > 0)
                         ? ((visibility > 0) ? 15 : 0)
                         : ((visibility > 0) ? 0 : 15);
    gl_TessLevelOuter[0] = u_level > 0 ? 8 : 1;
}

#elif defined(TESS_EVALUATION)

layout(isolines, point_mode) in;

layout(location = 0) flat out ivec2 v_cluster;
layout(location = 1) flat out vec4 v_view_normal;
layout(location = 2) flat out uvec2 v_cell;
layout(location = 3) flat out mat3 v_tbn;

void main()
{
    int idx2 = int(round(gl_TessCoord.x * gl_TessLevelOuter[1]));
    int idx = int(round(gl_TessCoord.y * gl_TessLevelOuter[0]));

    uvec3 metacell = texelFetch(u_metacells, gl_PrimitiveID * CLUSTER_SIZE + idx2).rgb;
    vec3 local_pos = vec3((metacell.xxy >> uvec3(0u, 16u, 0u)) & 65535u) * 2.0 + 0.5;
    uint mask = (metacell.y >> 16u) & 255u;
    int cell_id = u_level > 0 ? idx : (gl_PrimitiveID + u_scene.frame) % META_SIZE;
    int cell_offset = int(metacell.z + bitCount(mask & ~(0xffu << cell_id)));

    if (bool(u_shadows_enabled) && (u_level == 0 || cell_id == 0) && mask > 0u)
        splatShadowDepth(local_pos + 1.0);

    if ((mask & (1u << cell_id)) == 0u || idx2 == 15) DISCARD_VERTEX;

    vec4 c0 = (u_level > 0) ? texelFetch(u_cells, cell_offset * 2 + 0) : vec4(0.0);
    vec4 c1 = (u_level > 0) ? texelFetch(u_cells, cell_offset * 2 + 1) : vec4(0.0);
    vec3 local_normal = -normalize(cellGrad(c0, c1));
    local_pos += vec3((uvec3(cell_id) >> uvec3(0u, 1u, 2u)) & 1u);
    if (u_level == 0)
        local_pos += lds_r3((gl_PrimitiveID + cell_id + u_scene.frame) & 1023) - 0.5;

    vec4 view_pos = u_scene.view * (u_drawable.model * vec4(local_pos, 1.0));
    vec3 view_normal = normalize(mat3(u_scene.view) * local_normal);
    float view_radius = 0.5 * length(u_drawable.model[0].xyz);
    bool front_facing = u_level == 0 || dot(normalize(view_pos.xyz), view_normal) < 0.0;
    bool visible = front_facing && view_pos.z < 0.0;
    if (!visible) DISCARD_VERTEX;

    gl_PointSize = (-view_radius / view_pos.z) * u_scene.proj[1][1] * u_scene.res[1];
    gl_PointSize *= u_point_scale;
    if (u_level == 0)
        gl_PointSize = min(1.0, gl_PointSize);
    gl_Position = u_scene.proj * view_pos;

    v_cluster = ivec2(gl_PrimitiveID, 0);
    v_view_normal = vec4(bool(USE_VOLUME_SAMPLING) ? local_pos : view_normal, 0.0);
    v_cell = uvec2(packUnorm4x8(c0), packUnorm4x8(c1));
    v_tbn = u_point_scale * 0.95 * transpose(mat3(u_scene.view));
    v_tbn[2] = normalize(v_tbn * view_pos.xyz);
}

#elif defined(FRAGMENT)

layout(location = 0) flat in ivec2 v_cluster;
layout(location = 1) flat in vec4 v_view_normal;
layout(location = 2) flat in uvec2 v_cell;
layout(location = 3) flat in mat3 v_tbn;

layout(location = 0) out vec4 rt_gbuffer;
layout(location = 1) out vec2 rt_id;

// Branchless ray/AABB intersection test adapted from
// https://tavianator.com/fast-branchless-raybounding-box-intersections/
bool intersectUnitBox(vec3 ray_origin, vec3 ray_dir_inv, out float tmin, out float tmax)
{
    vec3 t1 = (-0.5 - ray_origin) * ray_dir_inv;
    vec3 t2 = (0.5 - ray_origin) * ray_dir_inv;
    tmin = max(min(t1[0], t2[0]), max(min(t1[1], t2[1]), min(t1[2], t2[2])));
    tmax = min(max(t1[0], t2[0]), min(max(t1[1], t2[1]), max(t1[2], t2[2])));

    return bool((tmax - tmin) > 0.0);
}

// Perform interval refinement from three samples
vec3 intervalRefinement(vec3 p0, vec3 p1, vec3 p2, float s0, float s1, float s2)
{
    bool side = step(0.5, s0) != step(0.5, s2);
    return mix(p2, (side ? p0 : p1), (0.5 - s2) / ((side ? s0 : s1) - s2));
}

vec3 imageGrad(vec3 p, vec3 delta)
{
    vec3 grad;
    grad.x = texture(u_volume, p + vec3(delta.x, 0, 0)).r - texture(u_volume, p - vec3(delta.x, 0, 0)).r,
    grad.y = texture(u_volume, p + vec3(0, delta.y, 0)).r - texture(u_volume, p - vec3(0, delta.y, 0)).r,
    grad.z = texture(u_volume, p + vec3(0, 0, delta.z)).r - texture(u_volume, p - vec3(0, 0, delta.z)).r;
    return clamp(grad, -9999.0, 9999.0);
}

bool intersectCell(vec3 ray_origin, vec3 ray_dir, float tmin, float tmax, inout vec3 view_normal)
{
    vec3 p0 = clamp(ray_origin + tmin * ray_dir + 0.5, 0.0, 1.0);
    vec3 p1 = clamp(ray_origin + tmax * ray_dir + 0.5, 0.0, 1.0);
    vec3 p2 = (p0 + p1) * 0.5;
#if USE_VOLUME_SAMPLING
    // Sample in the original volume data
    vec3 delta = 1.0 / textureSize(u_volume, 0).xyz;
    vec3 texcoord = v_view_normal.xyz * delta;
    float s0 = texture(u_volume, texcoord + p0 * delta).r;
    float s1 = texture(u_volume, texcoord + p1 * delta).r;
    float s2 = texture(u_volume, texcoord + p2 * delta).r;
    vec3 p = intervalRefinement(p0, p1, p2, s0, s1, s2);
    if (max(s0, max(s1, s2)) >= 0.5)
        view_normal = normalize(mat3(u_scene.view) * -imageGrad(texcoord + p * delta, delta));
#else
    // Sample stored grid cell
    vec4 c0 = unpackUnorm4x8(v_cell[0]);
    vec4 c1 = unpackUnorm4x8(v_cell[1]);
    float s0 = cellLinear(p0, c0, c1);
    float s1 = cellLinear(p1, c0, c1);
    float s2 = cellLinear(p2, c0, c1);
#endif

    return max(s0, max(s1, s2)) >= 0.5; // && min(s0, min(s1, s2)) < 0.5;
}

void main()
{
    vec3 view_normal = v_view_normal.xyz;
    float point_size = 1.0 / dFdx(gl_PointCoord.x);

#if USE_VOXEL_RASTERISATION
    if (u_level > 0 && point_size > 0.8) {
        vec2 uv = gl_PointCoord - 0.5;
        vec3 ray_origin = uv.x * v_tbn[0] - uv.y * v_tbn[1];
        vec3 ray_dir = v_tbn[2];
        float tmin, tmax;
        bool hit = intersectUnitBox(ray_origin, 1.0 / ray_dir, tmin, tmax);

    #if USE_CELL_RASTERISATION
        hit = hit && intersectCell(ray_origin, ray_dir, tmin, tmax, view_normal);
    #endif // USE_CELL_RASTERISATION
        if (!hit) discard;
    }
#endif // USE_VOXEL_RASTERISATION

    rt_gbuffer = vec4(view_normal, 1.0) * 0.1;
    rt_id = intBitsToFloat(ivec2(((v_cluster[0] + 1) << 1) | u_level, v_cluster[1]));
}

#endif
