#define META_SIZE 8
#define CLUSTER_SIZE 16
#define MAX_CLUSTERS (1 << 20)
#define USE_VOXEL_RASTERISATION 1
#define USE_CELL_RASTERISATION 1

#define OP_SPLAT_DEPTH 0
#define OP_SPLAT_GBUFFER 1
#define OP_INIT 2

layout(binding = 0, std140) uniform SceneUniforms {
    mat4 view;
    mat4 proj;
    vec2 res;
    float time;
    int frame;
} u_scene;

layout(binding = 1, std140) uniform DrawableUniforms {
    mat4 model;
    mat4 normal;
} u_drawable;

layout(binding = 0, std430) restrict buffer DrawBuffer {
    ivec4 cmds[4];
    ivec2 data[];
} u_draw_buffer;

layout(location = 0) uniform int u_level;
layout(location = 1) uniform int u_op;
layout(location = 2) uniform float u_point_scale;
layout(location = 3) uniform int u_shadows_enabled;
layout(location = 4) uniform mat4 u_shadow_from_view;
layout(binding = 0) uniform usamplerBuffer u_cells;
layout(binding = 1) uniform usamplerBuffer u_metacells;
layout(binding = 0, r32ui) uniform restrict uimage2D u_depth;
layout(binding = 1, rgba16f) uniform restrict writeonly image2D u_gbuffer;
layout(binding = 2, rg32ui) uniform restrict writeonly uimage2D u_id;
layout(binding = 3, r32ui) uniform restrict uimage2D u_shadowmap;

vec3 lds_r3(float n)
{
    float phi = 1.2207440846;
    return fract(n / vec3(phi, phi * phi, phi * phi * phi));
}

float cellLinear(vec3 p, vec4 c0, vec4 c1)
{
    return mix(mix(mix(c0[0], c0[1], p.x), mix(c0[2], c0[3], p.x), p.y),
               mix(mix(c1[0], c1[1], p.x), mix(c1[2], c1[3], p.x), p.y), p.z);
}

vec3 cellGrad(vec4 c0, vec4 c1)
{
    float grad_x = (c0[1] + c0[3] + c1[1] + c1[3]) - (c0[0] + c0[2] + c1[0] + c1[2]);
    float grad_y = (c0[2] + c0[3] + c1[2] + c1[3]) - (c0[0] + c0[1] + c1[0] + c1[1]);
    float grad_z = (c1[0] + c1[1] + c1[2] + c1[3]) - (c0[0] + c0[1] + c0[2] + c0[3]);
    return vec3(grad_x, grad_y, grad_z) * 0.25;
}

void splatShadowDepth(vec3 local_pos)
{
    vec4 clip_pos = u_shadow_from_view * (u_scene.view * (u_drawable.model * vec4(local_pos, 1.0)));
    vec3 ndc_pos = clip_pos.xyz / clip_pos.w;
    vec2 frag_pos = (ndc_pos.xy * 0.5 + 0.5) * vec2(512, 512);
    float frag_depth = ndc_pos.z * 0.5 + 0.5;
    imageAtomicMin(u_shadowmap, ivec2(frag_pos), floatBitsToUint(frag_depth));
}

// Branchless ray/bbox intersection test adapted from
// https://tavianator.com/fast-branchless-raybounding-box-intersections/
bool intersectUnitBox(vec3 ray_origin, vec3 ray_dir_inv, out float tmin, out float tmax)
{
    vec3 t1 = (-0.5 - ray_origin) * ray_dir_inv;
    vec3 t2 = (0.5 - ray_origin) * ray_dir_inv;
    tmin = max(min(t1[0], t2[0]), max(min(t1[1], t2[1]), min(t1[2], t2[2])));
    tmax = min(max(t1[0], t2[0]), min(max(t1[1], t2[1]), max(t1[2], t2[2])));

    return bool((tmax - tmin) > 0.0);
}

bool intersectCell(in vec4 c0, in vec4 c1, vec3 ray_origin, vec3 ray_dir, float tmin, float tmax)
{
    vec3 p0 = clamp(ray_origin + tmin * ray_dir + 0.5, 0.0, 1.0);
    vec3 p1 = clamp(ray_origin + tmax * ray_dir + 0.5, 0.0, 1.0);
    float s0 = cellLinear(mix(p0, p1, 0.0), c0, c1);
    float s1 = cellLinear(mix(p0, p1, 0.5), c0, c1);
    float s2 = cellLinear(mix(p0, p1, 1.0), c0, c1);

    return max(s0, max(s1, s2)) >= 0.5; // && min(s0, min(s1, s2)) < 0.5;
}

#if defined(COMPUTE)

layout(local_size_x = 64) in;

void main()
{
    int thread_id = int(gl_GlobalInvocationID.x);
    if (u_op == OP_INIT) {
        // Update parameters for indirect dispatches
        u_draw_buffer.cmds[2] = ivec4(u_draw_buffer.cmds[0].x / gl_WorkGroupSize.x, 1, 1, 0);
        u_draw_buffer.cmds[3] = ivec4(u_draw_buffer.cmds[1].x / gl_WorkGroupSize.x, 1, 1, 0);
        return;
    }

    int stride = (u_level > 0 ? META_SIZE : 1) * CLUSTER_SIZE;
    ivec2 cluster = u_draw_buffer.data[u_level * MAX_CLUSTERS + thread_id / stride];
    int meta_id = (u_level > 0 ? thread_id / META_SIZE : thread_id) % CLUSTER_SIZE;
    int cell_id = (u_level > 0 ? thread_id : u_scene.frame + meta_id) % META_SIZE;
    int meta_offset = cluster[0] * CLUSTER_SIZE + meta_id;
    int cell_offset = cluster[1];

    uvec2 metacell = texelFetch(u_metacells, meta_offset).rg;
    vec3 meta_local_pos = vec3((metacell.xxx >> uvec3(0u, 11u, 22u)) & 2047u) * 2.0 + 0.5;
    uint mask = (metacell.y >> 24u);
    cell_offset += int((metacell.y & 0xffffffu) + bitCount(mask & ~(0xffu << cell_id)));

    if (bool(u_shadows_enabled) && (u_level == 0 || cell_id == 0) && mask > 0u && u_op == OP_SPLAT_GBUFFER)
        splatShadowDepth(meta_local_pos + 1.0);

    if ((mask & (1u << cell_id)) == 0u)
        return;

    uvec2 cell = (u_level > 0) ? texelFetch(u_cells, cell_offset).rg : uvec2(0u);
    vec4 c0 = unpackUnorm4x8(cell[0]);
    vec4 c1 = unpackUnorm4x8(cell[1]);
    vec3 local_normal = -normalize(cellGrad(c0, c1));
    vec3 local_pos = meta_local_pos + vec3((uvec3(cell_id) >> uvec3(0u, 1u, 2u)) & 1u);
    if (u_level == 0)
        local_pos += lds_r3((cluster[0] + cell_id + u_scene.frame) & 1023) - 0.5;

    vec4 view_pos = u_scene.view * (u_drawable.model * vec4(local_pos, 1.0));
    vec3 view_normal = normalize(mat3(u_scene.view) * local_normal);
    float view_radius = 0.5 * length(u_drawable.model[0].xyz);
    bool front_facing = u_level == 0 || dot(normalize(view_pos.xyz), view_normal) < 0.01;
    bool visible = front_facing && view_pos.z < 0.0;

    float point_size = (-view_radius / view_pos.z) * u_scene.proj[1][1] * u_scene.res[1];
    point_size *= u_point_scale;
    float point_size2 = max(0.5, (u_level == 0) ? min(1.0, point_size) : point_size);
    if (!visible || point_size < 0.8 * fract((cell_id + META_SIZE * meta_id) * 1.618034))
        return;

    vec4 clip_pos = u_scene.proj * view_pos;
    vec3 ndc_pos = clip_pos.xyz / clip_pos.w;
    vec2 frag_pos = (ndc_pos.xy * 0.5 + 0.5) * u_scene.res;
    float frag_depth = ndc_pos.z * 0.5 + 0.5;

    float x_min = round(frag_pos.x - point_size2 * 0.5) + 0.5;
    float x_max = round(frag_pos.x + point_size2 * 0.5) + 0.5;
    float y_min = round(frag_pos.y - point_size2 * 0.5) + 0.5;
    float y_max = round(frag_pos.y + point_size2 * 0.5) + 0.5;

    mat3 tbn = u_point_scale * transpose(mat3(u_scene.view));
    tbn[2] = normalize(tbn * view_pos.xyz);

    for (float y = y_min; y < y_max; y += 1.0) {
        float v = (y - frag_pos.y) * (1.0 / point_size2);
        for (float x = x_min; x < x_max; x += 1.0) {
            float u = (x - frag_pos.x) * (1.0 / point_size2);
            ivec2 p = ivec2(x, y);

        #if USE_VOXEL_RASTERISATION
            if (u_level > 0 && point_size2 > 0.8) {
                vec3 ray_origin = u * tbn[0] + v * tbn[1];
                vec3 ray_dir = tbn[2];
                float tmin, tmax;
                bool hit = intersectUnitBox(ray_origin, 1.0 / ray_dir, tmin, tmax);
            #if USE_CELL_RASTERISATION
                hit = hit && intersectCell(c0, c1, ray_origin, ray_dir, tmin, tmax);
            #endif // USE_CELL_RASTERISATION
                if (!hit) continue;
            }
        #endif // USE_VOXEL_RASTERISATION

            if (u_op == OP_SPLAT_DEPTH) {
                imageAtomicMin(u_depth, p, floatBitsToUint(frag_depth));
            }
            if (u_op == OP_SPLAT_GBUFFER) {
                if (imageLoad(u_depth, p).r >= floatBitsToUint(frag_depth)) {
                    if (u_level > 0)
                        imageStore(u_gbuffer, p, vec4(view_normal, point_size));
                    imageStore(u_id, p, uvec4(((cluster[0] + 1) << 1) | u_level, cluster[1], 0u, 0u));
                }
            }
        }
    }
}

#endif
