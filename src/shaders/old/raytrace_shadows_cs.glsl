#define META_SIZE 8
#define CLUSTER_SIZE 16
#define BLOCKS_PER_SIDE 16
#define EPSILON 1e-5
#define HALF_EPSILON (0.5 * EPSILON)
#define MAX_BLOCK_STEPS 36
#define M_PI 3.141592

layout(binding = 0, std140) uniform SceneUniforms {
    mat4 view;
    mat4 proj;
    vec2 res;
    float time;
    int frame;
} u_scene;

layout(binding = 1, std140) uniform DrawableUniforms {
    mat4 model;
    mat4 normal;
} u_drawable;

layout(location = 0) uniform ivec3 u_volume_dim;
layout(binding = 0) uniform usamplerBuffer u_cells;
layout(binding = 1) uniform usamplerBuffer u_metacells;
layout(binding = 2) uniform usamplerBuffer u_blocks;
layout(binding = 3) uniform sampler2D u_bluenoise;
layout(binding = 0, r32ui) uniform restrict readonly uimage2D u_depth;
layout(binding = 1, rgba16f) uniform restrict readonly image2D u_gbuffer;
layout(binding = 2, rgba16f) uniform restrict writeonly image2D u_color;

vec3 reconstructViewPos(vec3 ndc_pos, mat4 proj)
{
    float a = proj[0][0];
    float b = proj[1][1];
    float c = proj[2][2];
    float d = proj[3][2];
    float e = proj[2][0];
    float f = proj[2][1];
    float view_pos_z = -d / (ndc_pos.z + c);
    float view_pos_x = (-view_pos_z / a) * (ndc_pos.x + e);
    float view_pos_y = (-view_pos_z / b) * (ndc_pos.y + f);
    return vec3(view_pos_x, view_pos_y, view_pos_z);
}

// https://tavianator.com/fast-branchless-raybounding-box-intersections/
bool intersectBox(vec3 aabb[2], vec3 ray_origin, vec3 ray_dir_inv, out float tmin, out float tmax)
{
    vec3 t1 = (aabb[0] - ray_origin) * ray_dir_inv;
    vec3 t2 = (aabb[1] - ray_origin) * ray_dir_inv;
    tmin = max(0.0, max(min(t1[0], t2[0]), max(min(t1[1], t2[1]), min(t1[2], t2[2]))));
    tmax = max(0.0, min(max(t1[0], t2[0]), min(max(t1[1], t2[1]), max(t1[2], t2[2]))));

    return bool((tmax - tmin) > 0.0);
}

bool intersectBlock(uvec4 block, vec3 ray_origin, vec3 ray_dir, out float t, int steps)
{
    int count = int(block.x);
    int first = int(block.y);
    if (count == 0) return false;

    vec3 aabb_cluster[2], aabb_metacell[2], aabb_cell[2];
    vec3 ray_dir_inv = clamp(1.0 / ray_dir, -9999.0, 9999.0);
    vec4 closest = vec4((ray_origin + 0.5) * (0.5 * u_volume_dim.x), 9999.0);
    float tmin, tmax;

    for (int k = 0; k < count && closest.w == 9999.0; ++k) {
        // Find next candidate cluster along the ray
        for (; k < count; ++k) {
            int offset = (first + k) * CLUSTER_SIZE;
            uvec2 cluster = texelFetch(u_metacells, offset + (CLUSTER_SIZE - 1)).rg;
            aabb_cluster[0] = vec3((cluster.xxx >> uvec3(0u, 11u, 22u)) & 2047u);
            aabb_cluster[1] = aabb_cluster[0] + vec3((cluster.yyy >> uvec3(0u, 8u, 16u)) & 255u) + 1.0;

            if (intersectBox(aabb_cluster, closest.xyz, ray_dir_inv, tmin, tmax) && tmin < closest.w)
                break;
        }

        int offset = (first + k) * CLUSTER_SIZE;
        for (int j = 0; j < (CLUSTER_SIZE - 1) && k < count; ++j) {
            uvec2 metacell = texelFetch(u_metacells, offset + j).rg;
            aabb_metacell[0] = vec3((metacell.xxx >> uvec3(0u, 11u, 22u)) & 2047u);
            aabb_metacell[1] = aabb_metacell[0] + 1.0;

            if (intersectBox(aabb_metacell, closest.xyz, ray_dir_inv, tmin, tmax) && tmin < closest.w) {
                if (steps > 1) { closest.w = tmin; continue; }

                for (int i = 0; i < META_SIZE; ++i) {
                    aabb_cell[0] = aabb_metacell[0] + vec3(i & 1, (i >> 1) & 1, i >> 2) * 0.5;
                    aabb_cell[1] = aabb_cell[0] + 0.5;

                    if (intersectBox(aabb_cell, closest.xyz, ray_dir_inv, tmin, tmax) && tmin < closest.w) {
                        if ((metacell.y & (1u << (24 + i))) > 0u)
                            closest.w = tmin;
                    }
                }
            }
        }
    }

    t = closest.w * (1.0 / (0.5 * u_volume_dim.x));
    return closest.w < 9999.0;
}

bool intersectBlockVolume(vec3 ray_origin, vec3 ray_dir, out float tmin, out float tmax)
{
    const int NUM_BLOCKS = BLOCKS_PER_SIDE * BLOCKS_PER_SIDE * BLOCKS_PER_SIDE;

    vec3 ray_dir_inv = clamp(1.0 / ray_dir, -9999.0, 9999.0);
    vec3 aabb_volume[2] = vec3[](vec3(-0.5 - HALF_EPSILON), vec3(0.5 + HALF_EPSILON));
    bool hit = intersectBox(aabb_volume, ray_origin, ray_dir_inv, tmin, tmax);
    bool inside = true;

    if (hit) {
        vec3 p = clamp(ray_origin + (tmin + EPSILON) * ray_dir + 0.5, 0.0, 0.9999);
        vec3 q = fract(p * BLOCKS_PER_SIDE) - 0.5;
        vec3 aabb_block[2] = vec3[](vec3(-0.5 - HALF_EPSILON), vec3(0.5 + HALF_EPSILON));
        uvec4 block = uvec4(0u);

        hit = false;
        for (int k = 0; k < MAX_BLOCK_STEPS && inside; ++k) {
            // Advance to next non-empty block
            for (; k < MAX_BLOCK_STEPS && inside; ++k) {
                int block_id = int(dot(floor(p * BLOCKS_PER_SIDE), vec3(1, 16, 256)));
                block = texelFetch(u_blocks, max(0, min(NUM_BLOCKS, block_id)));
                if (block.r > 0u) break;

                intersectBox(aabb_block, q, ray_dir_inv, tmin, tmax);
                p += (tmax + EPSILON) * (1.0 / BLOCKS_PER_SIDE) * ray_dir;
                q = fract(p * BLOCKS_PER_SIDE) - 0.5;
                inside = all(lessThan(abs(p - 0.5), vec3(0.5)));
            }

            if (hit = intersectBlock(block, ray_origin, ray_dir, tmin, k))
                break;

            intersectBox(aabb_block, q, ray_dir_inv, tmin, tmax);
            p += (tmax + EPSILON) * (1.0 / BLOCKS_PER_SIDE) * ray_dir;
            q = fract(p * BLOCKS_PER_SIDE) - 0.5;
            inside = all(lessThan(abs(p - 0.5), vec3(0.5)));
        }
    }

    return hit;
}

#if defined(COMPUTE)

layout(local_size_x = 8, local_size_y = 8) in;

void main()
{
    vec2 frag_pos = vec2(gl_WorkGroupID.xy * gl_WorkGroupSize.xy + gl_LocalInvocationID.xy) + 0.5;
    int frame = u_scene.frame % 1024;

    float frag_depth = uintBitsToFloat(imageLoad(u_depth, ivec2(frag_pos)).r);
    vec3 view_normal = imageLoad(u_gbuffer, ivec2(frag_pos)).rgb;
    float point_size = imageLoad(u_gbuffer, ivec2(frag_pos)).a;
    if (frag_depth == 1.0) return;

    vec3 ndc_pos = vec3(frag_pos / u_scene.res, frag_depth) * 2.0 - 1.0;
    vec3 view_pos = reconstructViewPos(ndc_pos, u_scene.proj);
    vec3 light_view_dir = normalize(vec3(0.5, 1.0, 2.0) - view_pos);

    mat4 local_from_view = inverse(u_scene.view);
    vec3 local_pos = vec3(local_from_view * vec4(view_pos, 1.0));
    vec3 local_normal = mat3(local_from_view) * view_normal;

    vec3 seed = texelFetch(u_bluenoise, ivec2(frag_pos) % 128, 0).rgb;
    vec3 rnd = fract(seed + vec3(sqrt(2.0), sqrt(3.0), sqrt(5.0)) * frame);
    float u = rnd.x * 2.0 - 1.0;
    float phi = rnd.y * 2.0 * M_PI;
    vec3 rnd_ball = sqrt(rnd.z) * vec3(vec2(cos(phi), sin(phi)) * sqrt(1.0 - u * u), u);

#if 1
    vec3 ray_dir = normalize(local_normal + rnd_ball);
    vec3 ray_origin = local_pos + 5e-3 * (local_normal + ray_dir);
#else
    vec3 ray_dir = mat3(local_from_view) * light_view_dir;
    vec3 ray_origin = local_pos + 1e-2 * (ray_dir + rnd_ball * 0.5);
#endif

    float tmin, tmax;
    bool hit = intersectBlockVolume(ray_origin, ray_dir, tmin, tmax);

    float visibility = 1.0 - float(hit);
    vec3 output_color = visibility * vec3((mat3(u_scene.view) * ray_dir).y * 0.5 + 0.5);

    imageStore(u_color, ivec2(frag_pos), vec4(output_color, point_size));
}

#endif
