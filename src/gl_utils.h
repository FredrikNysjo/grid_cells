/// @file    gl_utils.h
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2018 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.

#pragma once

#include <GL/glew.h>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtx/constants.hpp>
#include <glm/gtx/quaternion.hpp>

#include <cstdint>
#include <string>
#include <vector>

struct Material {
    glm::vec4 albedo;
    float gloss;
    float metallic;
    float wrap;
    float pad[1];

    Material() { albedo = glm::vec4(0.95f); gloss = 0.4f; metallic = 0.0f; wrap = 0.2f; }
};

struct HemisphereLight {
    glm::vec4 up;
    glm::vec4 ground_color;
    glm::vec4 sky_color;
    float intensity;
    float pad[3];

    HemisphereLight() { up = glm::vec4(0.0f, 1.0f, 0.0f, 0.0f); ground_color = glm::vec4(0.1f), sky_color = glm::vec4(1.0); intensity = 0.65f; }
};

struct Light {
    glm::vec4 position;
    glm::vec4 color;
    float intensity;
    float pad[3];

    Light() { position = glm::vec4(1.0f, 2.0f, 3.0f, 1.0f); color = glm::vec4(1.0f); intensity = 0.5f; }
};

struct GLTimerQuery {
    GLuint query;
    GLuint64 result;

    GLTimerQuery() { query = 0; result = 0; }
};

namespace cg {

/// Struct for representing a virtual 3D trackball that can be used for
/// object or camera rotation
struct CgTrackball {
    double radius;
    glm::vec2 center;
    bool tracking;
    glm::vec3 vStart;
    glm::quat qStart;
    glm::quat qCurrent;

    CgTrackball()
    {
        radius = 1.0;
        center = glm::vec2(0.0f, 0.0f);
        tracking = false;
        vStart = glm::vec3(0.0f, 0.0f, 1.0f);
        qStart = glm::quat(1.0f, 0.0f, 0.0f, 0.0f);
        qCurrent = glm::quat(1.0f, 0.0f, 0.0f, 0.0f);
    }
};

/// Returns the value of an environment variable
const char *cgGetEnvSafe(const char *name);

/// Load and compile GLSL shader from file
GLuint cgLoadShader(const std::string &filename, GLenum shaderType);

/// Create GLSL program from compiled shaders
GLuint cgCreateShaderProgram(const std::vector<GLuint> &shaders);

/// Load GLSL program from a single file that contains multiple shader stages
GLuint cgLoadShaderProgram(const std::string &filename,
                           const std::string &version="#version 450");

/// Start trackball tracking
void cgTrackballStartTracking(CgTrackball &trackball, const glm::vec2 &point);

/// Stop trackball tracking
void cgTrackballStopTracking(CgTrackball &trackball);

/// Rotate trackball from, e.g., mouse movement
void cgTrackballMove(CgTrackball &trackball, const glm::vec2 &point);

/// Get trackball orientation in matrix form
glm::mat4 cgTrackballGetRotationMatrix(CgTrackball &trackball);

} // namespace cg
