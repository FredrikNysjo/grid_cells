/// @file    gl_utils.cpp
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2018 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.

#include "gl_utils.h"

#include <cmath>
#include <cstring>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>

namespace cg {

const char *cgGetEnvSafe(const char *name)
{
    char const *value = std::getenv(name);
    return (value != nullptr) ? value : "";
}

std::string cgReadShaderSource(const std::string &filename)
{
    std::ifstream file(filename);
    std::stringstream stream;
    stream << file.rdbuf();

    return stream.str();
}

void cgShowShaderInfoLog(GLuint shader)
{
    GLint infoLogLength = 0;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);

    std::vector<char> infoLog(infoLogLength);
    glGetShaderInfoLog(shader, infoLogLength, &infoLogLength, &infoLog[0]);

    std::string infoLogStr(infoLog.begin(), infoLog.end());
    std::cerr << infoLogStr << std::endl;
}

void cgShowProgramInfoLog(GLuint program)
{
    GLint infoLogLength = 0;
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);

    std::vector<char> infoLog(infoLogLength);
    glGetProgramInfoLog(program, infoLogLength, &infoLogLength, &infoLog[0]);

    std::string infoLogStr(infoLog.begin(), infoLog.end());
    std::cerr << infoLogStr << std::endl;
}

GLuint cgLoadShader(const std::string &filename, GLenum shaderType)
{
    // Load shader source from file
    std::string source = cgReadShaderSource(filename);

    GLuint shader = glCreateShader(shaderType);
    const char *sourcePtr = source.c_str();
    glShaderSource(shader, 1, &sourcePtr, nullptr);
    glCompileShader(shader);

    GLint compiled = 0;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
    if (!compiled) {
        std::cerr << "Shader compilation failed:" << std::endl;
        cgShowShaderInfoLog(shader);
        glDeleteShader(shader);
        return 0;
    }

    return shader;
}

GLuint cgCreateShaderProgram(const std::vector<GLuint> &shaders)
{
    GLuint program = glCreateProgram();
    for (const auto shader : shaders) {
        glAttachShader(program, shader);
    }

    glLinkProgram(program);

    // Mark shaders for deletion after program is deleted
    for (const auto shader : shaders) {
        glDeleteShader(shader);
    }

    GLint linked = 0;
    glGetProgramiv(program, GL_LINK_STATUS, &linked);
    if (!linked) {
        std::cerr << "Linking failed:" << std::endl;
        cgShowProgramInfoLog(program);
        glDeleteProgram(program);
        return 0;
    }

    return program;
}

GLuint cgLoadShaderProgram(const std::string &filename, const std::string &version)
{
    const GLenum stages[] = { GL_VERTEX_SHADER, GL_GEOMETRY_SHADER, GL_FRAGMENT_SHADER,
        GL_TESS_CONTROL_SHADER, GL_TESS_EVALUATION_SHADER, GL_COMPUTE_SHADER };
    const char *tokens[] = { "defined(VERTEX)", "defined(GEOMETRY)", "defined(FRAGMENT)",
        "defined(TESS_CONTROL)", "defined(TESS_EVALUATION)", "defined(COMPUTE)" };
    const char *macros[] = { "\n#define VERTEX\n", "\n#define GEOMETRY\n", "\n#define FRAGMENT\n",
        "\n#define TESS_CONTROL\n", "\n#define TESS_EVALUATION\n", "\n#define COMPUTE\n" };

    // Load multi-shader source from file
    std::string multi_source = cgReadShaderSource(filename);

    GLuint program = glCreateProgram();
    for (int i = 0; i < (sizeof(stages) / sizeof(stages[0])); ++i) {
        if (!std::strstr(multi_source.c_str(), tokens[i])) { continue; }

        GLuint shader = glCreateShader(stages[i]);
        const char *source[] = { version.c_str(), macros[i], multi_source.c_str() };
        glShaderSource(shader, 3, source, nullptr);

        GLint compiled = 0;
        glCompileShader(shader);
        glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
        if (!compiled) {
            cgShowShaderInfoLog(shader);
            glDeleteShader(shader);
            continue;
        }

        glAttachShader(program, shader);
        glDeleteShader(shader);
    }

    GLint linked = 0;
    glLinkProgram(program);
    glGetProgramiv(program, GL_LINK_STATUS, &linked);
    if (!linked) {
        cgShowProgramInfoLog(program);
        glDeleteProgram(program);
        program = 0;
    }

    return program;
}

// Helper functions
namespace {
glm::vec3 mapMousePointToUnitSphere(const glm::vec2 &point, double radius, const glm::vec2 &center)
{
    float x = point[0] - center[0];
    float y = -point[1] + center[1];
    float z = 0.0f;
    if (x * x + y * y < radius * radius / 2.0f) {
        z = std::sqrt(radius * radius - (x * x + y * y));
    }
    else {
        z = (radius * radius / 2.0f) / std::sqrt(x * x + y * y);
    }
    return glm::normalize(glm::vec3(x, y, z));
}

void cgComputeNormals(const std::vector<glm::vec3> &vertices,
                      const std::vector<std::int32_t> &indices,
                      std::vector<glm::vec3> *normals)
{
    normals->resize(vertices.size(), glm::vec3(0.0f, 0.0f, 0.0f));

    // Compute per-vertex normals by averaging the unnormalized face normals
    std::int32_t vertexIndex0, vertexIndex1, vertexIndex2;
    glm::vec3 normal;
    int numIndices = indices.size();
    for (int i = 0; i < numIndices; i += 3) {
        vertexIndex0 = indices[i];
        vertexIndex1 = indices[i + 1];
        vertexIndex2 = indices[i + 2];
        normal = glm::cross(vertices[vertexIndex1] - vertices[vertexIndex0],
                            vertices[vertexIndex2] - vertices[vertexIndex0]);
        (*normals)[vertexIndex0] += normal;
        (*normals)[vertexIndex1] += normal;
        (*normals)[vertexIndex2] += normal;
    }

    int numNormals = normals->size();
    for (int i = 0; i < numNormals; i++) {
        (*normals)[i] = glm::normalize((*normals)[i]);
    }
}
} // namespace

void cgTrackballStartTracking(CgTrackball &trackball, const glm::vec2 &point)
{
    trackball.vStart = mapMousePointToUnitSphere(point, trackball.radius, trackball.center);
    trackball.qStart = glm::quat(trackball.qCurrent);
    trackball.tracking = true;
}

void cgTrackballStopTracking(CgTrackball &trackball)
{
    trackball.tracking = false;
}

void cgTrackballMove(CgTrackball &trackball, const glm::vec2 &point)
{
    glm::vec3 vCurrent = mapMousePointToUnitSphere(point, trackball.radius, trackball.center);
    glm::vec3 rotationAxis = glm::cross(trackball.vStart, vCurrent);
    float dotProduct = std::max(std::min(glm::dot(trackball.vStart, vCurrent), 1.0f), -1.0f);
    float rotationAngle = std::acos(dotProduct);
    float eps = 0.01f;
    if (rotationAngle < eps) {
        trackball.qCurrent = glm::quat(trackball.qStart);
    }
    else {
        // Note: here we provide rotationAngle in radians. Older versions
        // of GLM (0.9.3 or earlier) require the angle in degrees.
        glm::quat q = glm::angleAxis(rotationAngle, rotationAxis);
        q = glm::normalize(q);
        trackball.qCurrent = glm::normalize(glm::cross(q, trackball.qStart));
    }
}

glm::mat4 cgTrackballGetRotationMatrix(CgTrackball &trackball)
{
    return glm::mat4_cast(trackball.qCurrent);
}

} // namespace cg
