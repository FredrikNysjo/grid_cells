/// @file    gl_state.h
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2017 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.

#pragma once

#include <GL/glew.h>

#include <array>

struct GLPipelineStateInfo {
    struct GLProgramStateInfo {
        GLint current_program = 0;
        GLbitfield modified = 0u;

        void capture()
        {
            const GLProgramStateInfo defaults;

            glGetIntegerv(GL_CURRENT_PROGRAM, &current_program);
            if (defaults.current_program != current_program) {
                modified |= (1u << 0u);
            }
        }

        void apply() const
        {
            if (modified & (1u << 0u)) {
                glUseProgram(current_program);
            }
        }
    } program_state;

    struct GLRasterizationStateInfo {
        GLint rasterizer_discard = GL_FALSE;
        GLint program_point_size = GL_FALSE;
        GLint cull_face = GL_FALSE;
        GLbitfield modified = 0u;

        void capture()
        {
            const GLRasterizationStateInfo defaults;

            glGetIntegerv(GL_RASTERIZER_DISCARD, &rasterizer_discard);
            if (defaults.rasterizer_discard != rasterizer_discard) {
                modified |= (1u << 0u);
            }
            glGetIntegerv(GL_PROGRAM_POINT_SIZE, &program_point_size);
            if (defaults.program_point_size != program_point_size) {
                modified |= (1u << 1u);
            }
            glGetIntegerv(GL_CULL_FACE, &cull_face);
            if (defaults.cull_face != cull_face) {
                modified |= (1u << 2u);
            }
        }

        void apply() const
        {
            if (modified & (1u << 0u)) {
                if (rasterizer_discard) glEnable(GL_RASTERIZER_DISCARD); else glDisable(GL_RASTERIZER_DISCARD);
            }
            if (modified & (1u << 1u)) {
                if (program_point_size) glEnable(GL_PROGRAM_POINT_SIZE); else glDisable(GL_PROGRAM_POINT_SIZE);
            }
            if (modified & (1u << 2u)) {
                if (cull_face) glEnable(GL_CULL_FACE); else glDisable(GL_CULL_FACE);
            }
        }
    } rasterization_state;

    struct GLMultisampleStateInfo {
        GLint multisample = GL_TRUE;
        GLint sample_alpha_to_coverage = GL_FALSE;
        GLbitfield modified = 0u;

        void capture()
        {
            const GLMultisampleStateInfo defaults;

            glGetIntegerv(GL_MULTISAMPLE, &multisample);
            if (defaults.multisample != multisample) {
                modified |= (1u << 0u);
            }
            glGetIntegerv(GL_SAMPLE_ALPHA_TO_COVERAGE, &sample_alpha_to_coverage);
            if (defaults.sample_alpha_to_coverage != sample_alpha_to_coverage) {
                modified |= (1u << 1u);
            }
        }

        void apply() const
        {
            if (modified & (1u << 0u)) {
                if (multisample) glEnable(GL_MULTISAMPLE); else glDisable(GL_MULTISAMPLE);
            }
            if (modified & (1u << 1u)) {
                if (sample_alpha_to_coverage) glEnable(GL_SAMPLE_ALPHA_TO_COVERAGE); else glDisable(GL_SAMPLE_ALPHA_TO_COVERAGE);
            }
        }
    } multisample_state;

    struct GLDepthStencilStateInfo {
        GLint depth_test = GL_FALSE;
        GLint depth_writemask = GL_TRUE;
        std::array<GLfloat, 2> depth_range;
        GLbitfield modified = 0u;

        GLDepthStencilStateInfo() { depth_range = {{0.0f, 1.0f}}; }

        void capture()
        {
            const GLDepthStencilStateInfo defaults;

            glGetIntegerv(GL_DEPTH_TEST, &depth_test);
            if (defaults.depth_test != depth_test) {
                modified |= (1u << 0u);
            }
            glGetIntegerv(GL_DEPTH_WRITEMASK, &depth_writemask);
            if (defaults.depth_writemask != depth_writemask) {
                modified |= (1u << 1u);
            }
            glGetFloatv(GL_DEPTH_RANGE, &depth_range[0]);
            if (defaults.depth_range[0] != depth_range[0] ||
                defaults.depth_range[1] != depth_range[1]) {
                modified |= (1u << 2u);
            }
        }

        void apply() const
        {
            if (modified & (1u << 0u)) {
                if (depth_test) glEnable(GL_DEPTH_TEST); else glDisable(GL_DEPTH_TEST);
            }
            if (modified & (1u << 1u)) {
                glDepthMask(depth_writemask);
            }
            if (modified & (1u << 2u)) {
                glDepthRange(depth_range[0], depth_range[1]);
            }
        }
    } depth_stencil_state;

    struct GLBlendStateInfo {
        GLint blend = GL_FALSE;
        GLenum blend_src_rgb = GL_ONE;
        GLenum blend_dst_rgb = GL_ZERO;
        GLenum blend_equation_rgb = GL_FUNC_ADD;
        GLenum blend_src_alpha = GL_ONE;
        GLenum blend_dst_alpha = GL_ZERO;
        GLenum blend_equation_alpha = GL_FUNC_ADD;
        std::array<GLint, 4> color_writemask;
        GLbitfield modified = 0u;

        GLBlendStateInfo() { color_writemask = {{GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE}}; }

        void capture()
        {
            const GLBlendStateInfo defaults;

            glGetIntegerv(GL_BLEND, &blend);
            if (defaults.blend != blend) {
                modified |= (1u << 0u);
            }
            glGetIntegerv(GL_BLEND_SRC_RGB, (GLint *)&blend_src_rgb);
            glGetIntegerv(GL_BLEND_DST_RGB, (GLint *)&blend_dst_rgb);
            glGetIntegerv(GL_BLEND_SRC_ALPHA, (GLint *)&blend_src_alpha);
            glGetIntegerv(GL_BLEND_DST_ALPHA, (GLint *)&blend_dst_alpha);
            if (defaults.blend_src_rgb != blend_src_rgb ||
                defaults.blend_dst_rgb != blend_dst_rgb ||
                defaults.blend_src_alpha != blend_src_alpha ||
                defaults.blend_dst_alpha != blend_dst_alpha) {
                modified |= (1u << 1u);
            }
            glGetIntegerv(GL_BLEND_EQUATION_RGB, (GLint *)&blend_equation_rgb);
            glGetIntegerv(GL_BLEND_EQUATION_ALPHA, (GLint *)&blend_equation_alpha);
            if (defaults.blend_equation_rgb != blend_equation_rgb ||
                defaults.blend_equation_alpha != blend_equation_alpha) {
                modified |= (1u << 2u);
            }
            glGetIntegerv(GL_COLOR_WRITEMASK, &color_writemask[0]);
            if (defaults.color_writemask[0] != color_writemask[0] ||
                defaults.color_writemask[1] != color_writemask[1] ||
                defaults.color_writemask[2] != color_writemask[2] ||
                defaults.color_writemask[3] != color_writemask[3]) {
                modified |= (1u << 3u);
            }
        }

        void apply() const
        {
            if (modified & (1u << 0u)) {
                if (blend) glEnable(GL_BLEND); else glDisable(GL_BLEND);
            }
            if (modified & (1u << 1u)) {
                glBlendFuncSeparate(blend_src_rgb, blend_dst_rgb,
                                    blend_src_alpha, blend_dst_alpha);
            }
            if (modified & (1u << 2u)) {
                glBlendEquationSeparate(blend_equation_rgb, blend_equation_alpha);
            }
            if (modified & (1u << 3u)) {
                glColorMask(color_writemask[0], color_writemask[1],
                            color_writemask[2], color_writemask[3]);
            }
        }
    } blend_state;

    void capture()
    {
        program_state.capture();
        rasterization_state.capture();
        multisample_state.capture();
        depth_stencil_state.capture();
        blend_state.capture();
    }

    void apply() const
    {
        program_state.apply();
        rasterization_state.apply();
        multisample_state.apply();
        depth_stencil_state.apply();
        blend_state.apply();
    }
};

void glCaptureCurrentPipelineState(GLPipelineStateInfo *state_info)
{
    *state_info = GLPipelineStateInfo();
    state_info->capture();
}

void glClearCurrentPipelineState()
{
    GLPipelineStateInfo defaults;
    defaults.program_state.modified = ~0u;
    defaults.rasterization_state.modified = ~0u;
    defaults.multisample_state.modified = ~0u;
    defaults.depth_stencil_state.modified = ~0u;
    defaults.blend_state.modified = ~0u;
    defaults.apply();
}

void glUsePipelineState(const GLPipelineStateInfo &state_info)
{
    state_info.apply();
}
